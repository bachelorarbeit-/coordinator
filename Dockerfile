FROM maven:latest AS build
ARG BASE=/usr/src/app
COPY pom.xml ${BASE}/
COPY src ${BASE}/src
RUN mvn -f ${BASE}/pom.xml install -DskipTests

FROM openjdk:latest
COPY --from=build /usr/src/app/target/coordinator-0.0.1-SNAPSHOT.jar /app/map.jar
COPY --from=build /usr/src/app/target/lib/* /app/lib/

EXPOSE 8080
ENTRYPOINT ["java","-jar","/app/map.jar"]
