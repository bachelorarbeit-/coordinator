package ch.hsr.ba.coordinator;

import ch.hsr.ba.coordinator.controllers.rest.dtos.RegisterDto;
import ch.hsr.ba.coordinator.controllers.rest.trustee.dtos.ComponentDataDto;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.data.model.vote.VoteTrustee;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TrusteeTest extends ApplicationTest {
	private VoteExecution voteExecution = null;

	@Test
	public void getApiKey() throws Exception {
		String uri = "/trustee/register";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON_VALUE).content(super.mapToJson(new ComponentDataDto("localhost", (short) 8090))).contentType(MediaType.APPLICATION_JSON)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		RegisterDto registerTrustee = this.mapFromJson(content, RegisterDto.class);
		assertEquals(registerTrustee.getApiKey().length() > 0, true);
	}

	@Test
	public void registerToVote() throws Exception {
		String uri = "/trustee/register";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON_VALUE).content(super.mapToJson(new ComponentDataDto("localhost", (short) 8090))).contentType(MediaType.APPLICATION_JSON)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		RegisterDto registerTrustee = this.mapFromJson(content, RegisterDto.class);

		VoteExecution voteExecution = this.getOrCreateVote();
		uri = "/trustee/registerToVote/" + voteExecution.getId();
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).header("Authorization", "Bearer " + registerTrustee.getApiKey()).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}

	@Test
	public void sendPublicKeys() throws Exception {
		String uri = "/trustee/register";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON_VALUE).content(super.mapToJson(new ComponentDataDto("localhost", (short) 8090))).contentType(MediaType.APPLICATION_JSON)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		RegisterDto registerTrustee = this.mapFromJson(content, RegisterDto.class);

		VoteExecution voteExecution = this.getOrCreateVote();
		uri = "/trustee/registerToVote/" + voteExecution.getId();
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).header("Authorization", "Bearer " + registerTrustee.getApiKey()).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
		content = mvcResult.getResponse().getContentAsString();
		VoteExecution ve = this.mapFromJson(content, VoteExecution.class);

		for (VoteTrustee voteTrustee : ve.getTrustees()) {
			if (voteTrustee.getTrustee().getId() == registerTrustee.getId()) {
				List<LweSample> samples = generateRandomSamples(voteExecution, voteTrustee);
				uri = "/trustee/publishKeys/" + voteExecution.getId();
				mvc.perform(MockMvcRequestBuilders.post(uri).header("Authorization", "Bearer " + registerTrustee.getApiKey()).accept(MediaType.APPLICATION_JSON_VALUE).content(super.mapToJson(samples)).contentType(MediaType.APPLICATION_JSON)).andReturn();
				int status = mvcResult.getResponse().getStatus();
				assertEquals(200, status);
			}
		}
	}

	private List<LweSample> generateRandomSamples(VoteExecution voteExecution, VoteTrustee voteTrustee) {
		List<LweSample> samples = new ArrayList<>();
		Random random = new Random();
		for (int i = 0; i < voteExecution.getVoteParams().getGeneratedKeys(); i++) {
			Integer[] a = new Integer[voteExecution.getVoteParams().getSn()];
			for (int n = 0; n < voteExecution.getVoteParams().getSn(); n++) {
				a[n] = random.nextInt();
			}
			samples.add(new LweSample(0, voteTrustee, a, random.nextInt()));
		}
		return samples;
	}
}
