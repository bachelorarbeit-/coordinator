package ch.hsr.ba.coordinator;

import ch.hsr.ba.coordinator.controllers.rest.dtos.RegisterDto;
import ch.hsr.ba.coordinator.controllers.rest.trustee.dtos.ComponentDataDto;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class BulletinBoardTest extends ApplicationTest {
	@Test
	public void getApiKey() throws Exception {
		String uri = "/bulletinBoard/register";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON).content(super.mapToJson(new ComponentDataDto("localhost", (short) 8090))).contentType(MediaType.APPLICATION_JSON)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);

		String content = mvcResult.getResponse().getContentAsString();
		RegisterDto registerTrustee = this.mapFromJson(content, RegisterDto.class);
		assertThat(registerTrustee.getApiKey().length()).isGreaterThan(0);
	}

	@Test
	public void registerToVote() throws Exception {
		String uri = "/bulletinBoard/register";
		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).accept(MediaType.APPLICATION_JSON).content(super.mapToJson(new ComponentDataDto("localhost", (short) 8090))).contentType(MediaType.APPLICATION_JSON)).andReturn();
		String content = mvcResult.getResponse().getContentAsString();
		RegisterDto registerTrustee = this.mapFromJson(content, RegisterDto.class);

		VoteExecution voteExecution = this.getOrCreateVote();
		uri = "/bulletinBoard/registerToVote/" + voteExecution.getId();
		mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).header("Authorization", "Bearer " + registerTrustee.getApiKey()).accept(MediaType.APPLICATION_JSON)).andReturn();
		int status = mvcResult.getResponse().getStatus();
		assertEquals(200, status);
	}
}
