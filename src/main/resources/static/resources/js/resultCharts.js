async function getResults() {
	const url = `http://${window.location.hostname}:${window.location.port}/website/results`;

	const response = await fetch(url, {
		method: 'GET'
	});

	return await response.json();
}

async function populateChart() {
	try {
		const results = await getResults();

		if (typeof results !== 'undefined' && results.length > 0) {

			for (const currentChart of results) {

				var ctx = document.getElementById("chart" + currentChart.id).getContext('2d');
				var myChart = new Chart(ctx, {
					type: 'pie',
					data: {
						labels: currentChart.labels,
						datasets: [{
							backgroundColor: [
								"#2ecc71",
								"#6acccc",
								"#3498db",
								"#2e00db",
								"#939ba6",
								"#9b59b6",
								"#b100b6",
								"#edf10b",
								"#f1c40f",
								"#e74c3c",
								"#34495e"
							],
							data: currentChart.data
						}]
					}
				});
			}
		}
	} catch (error) {
		document.getElementById("resultContent").style.display = 'none';
	}
}

window.addEventListener("DOMContentLoaded", function () {

	populateChart();
}, false);
