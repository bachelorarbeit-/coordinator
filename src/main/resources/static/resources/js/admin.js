
$(document).ready(function () {
    let formatDateStringFrom = "HH:mm DD-MM-YYYY";
    let formatDateStringTo = "YYYY-MM-DD[T]HH:mm";

    addChoiceListener($("#addVote .choice"));
	setDate($("input[type=datetime-local]"), formatDateStringTo);

	$("form").submit(function (event) {
		event.preventDefault();
		let data = $(this).serializeObject();
		let choices = [];
		for (let i = 0; i < data.choices.length; i++) {
			if (data.choices[i].length > 0) {
				choices.push({id: 0, order: (choices.length + 1), option: data.choices[i]});
			}
		}
		data.choices = choices;

        fixDateFormat(data, formatDateStringFrom, formatDateStringTo);

        data.voteParams.counterLength = data.choices.length;

		$.ajax({
			contentType: 'application/json',
			data: JSON.stringify(data),
			dataType: 'json',
			success: function (data) {
				window.location.replace("/admin/");
			},
			error: function () {
				window.location.replace("/admin/");
			},
			processData: false,
			type: 'POST',
			url: $("form").attr("action")
		});
	});
});

function addChoiceListener(choiceInputs) {
	$(choiceInputs).each(function () {
		$(this).unbind();
		$(this).keyup(function () {
			let nbEmpty = 0;
			let addVoteChoiceSelector = $("#addVote .choice");
            addVoteChoiceSelector.each(function () {
				nbEmpty += ($(this).val().length === 0) ? 1 : 0;
			});
			if (nbEmpty === 0) {
                addVoteChoiceSelector.parent().append("<input type='text' name='choices[]' class='choice'/>");
				addChoiceListener($("#addVote .choice"));
			} else if (nbEmpty > 1) {
                addVoteChoiceSelector.each(function () {
					if ($(this).val().length === 0 && nbEmpty > 1) {
						nbEmpty--;
						$(this).remove();
					}
				});
				addChoiceListener(addVoteChoiceSelector);
			}
		});
	});
}

function setDate(elements, formatDateStringTo) {
	$(elements).each(function () {
		if ($(this).val() === "") {
			let date = new Date();
			if ($(this).attr("data-offset") !== null) {
				date = new Date(date.getTime() + 60 * 60 * 1000 * parseInt($(this).attr("data-offset")));
			}
			date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 0, 0);
            $(this).val(moment(date).format(formatDateStringTo));
		}
	})
}

function pad(num, size) {
	const s = "000000000" + num;
	return s.substr(s.length - size);
}

function fixDateFormat(data, formatDateStringFrom, formatDateStringTo) {
    let regex = RegExp("T");
    if (!regex.test(data.voteParams.voteStart)) {
        data.voteParams.voteStart = moment(data.voteParams.voteStart, formatDateStringFrom).format(formatDateStringTo);
        data.voteParams.voteEnd = moment(data.voteParams.voteEnd, formatDateStringFrom).format(formatDateStringTo);
    }
}

(function ($) {
	$.fn.serializeObject = function () {

		let self = this,
			json = {},
			push_counters = {},
			patterns = {
				"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
				"key": /[a-zA-Z0-9_]+|(?=\[\])/g,
				"push": /^$/,
				"fixed": /^\d+$/,
				"named": /^[a-zA-Z0-9_]+$/
			};


		this.build = function (base, key, value) {
			base[key] = value;
			return base;
		};

		this.push_counter = function (key) {
			if (push_counters[key] === undefined) {
				push_counters[key] = 0;
			}
			return push_counters[key]++;
		};

		$.each($(this).serializeArray(), function () {

			// skip invalid keys
			if (!patterns.validate.test(this.name)) {
				return;
			}

			let k,
				keys = this.name.match(patterns.key),
				merge = this.value,
				reverse_key = this.name;

			while ((k = keys.pop()) !== undefined) {

				// adjust reverse_key
				reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

				// push
				if (k.match(patterns.push)) {
					merge = self.build([], self.push_counter(reverse_key), merge);
				}

				// fixed
				else if (k.match(patterns.fixed)) {
					merge = self.build([], k, merge);
				}

				// named
				else if (k.match(patterns.named)) {
					merge = self.build({}, k, merge);
				}
			}

			json = $.extend(true, json, merge);
		});

		return json;
	};
})(jQuery);