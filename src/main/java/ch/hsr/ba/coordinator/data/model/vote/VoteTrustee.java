package ch.hsr.ba.coordinator.data.model.vote;

import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "vote_trustee", uniqueConstraints = {@UniqueConstraint(columnNames = {"trustee_id", "vote_execution_id"})})
public class VoteTrustee {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	private final int trusteeOrder;

	@ManyToOne
	private final Trustee trustee;
	@ManyToOne
	private final VoteExecution voteExecution;

	@OneToMany(mappedBy = "voteTrustee", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private final List<LweSample> lweSamples;

	boolean ready;

	public VoteTrustee() {
		this.id = 0;
		this.trusteeOrder = 0;
		this.trustee = null;
		this.voteExecution = null;
		this.lweSamples = new ArrayList<>();
		this.ready = false;
	}

	public VoteTrustee(long id, int order, Trustee trustee, VoteExecution voteExecution, List<LweSample> lweSamples, boolean ready) {
		this.id = id;
		this.trusteeOrder = order;
		this.trustee = trustee;
		this.voteExecution = voteExecution;
		this.lweSamples = lweSamples;
		this.ready = ready;
	}

	public long getId() {
		return id;
	}

	public int getTrusteeOrder() {
		return trusteeOrder;
	}

	public Trustee getTrustee() {
		return trustee;
	}

	@JsonIgnore
	public VoteExecution getVoteExecution() {
		return voteExecution;
	}

	@JsonIgnore
	public List<LweSample> getLweSamples() {
		return lweSamples;
	}

	@JsonIgnore
	public boolean isReady() {
		return this.ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}
}
