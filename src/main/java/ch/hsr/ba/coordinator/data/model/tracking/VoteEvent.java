package ch.hsr.ba.coordinator.data.model.tracking;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Calendar;

@Entity
@Table(name = "vote_event")
@DiscriminatorColumn(name = "origin")
public abstract class VoteEvent {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;
	@ManyToOne
	private final Vote vote;
	private final Calendar time;
	private final String type;

	public VoteEvent() {
		this.id = 0;
		this.vote = null;
		this.time = null;
		this.type = null;
	}

	public VoteEvent(long id, Vote vote, Calendar time, String type) {
		this.id = id;
		this.vote = vote;
		this.time = time;
		this.type = type;
	}

	public long getId() {
		return id;
	}

	public Vote getVote() {
		return vote;
	}

	public Calendar getTime() {
		return time;
	}

	public String getType() {
		return type;
	}
}
