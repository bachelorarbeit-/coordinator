package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@Entity
@Table(name = "vote_params")
public class VoteParams {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;
	@OneToOne
	private final VoteExecution vote;

	private int trustees;                // number of trustees
	private int bulletinBoards;                // number of bulletin boards
	private int counterLength;

	private int k;

	private double ksStdev;             // standard deviation
	private double bkStdev;             // standard deviation
	private double maxStdev;            //max standard deviation for a 1/4 msg space

	private int sn;                      // LWE modulus
	private int hLWE;                    // HW secret key LWE --> not used
	private double stdevLWE;             // LWE ciphertexts standard deviation
	private long bksBit;                 // Base bit key switching
	private int dks;                     // dimension key switching
	private double stdevKS;              // KS key standard deviation
	private int ln;                      // RLWE,RGSW modulus
	private int hRLWE;                   // HW secret key RLWE,RGSW --> not used
	private double stdevRLWEkey;         // 0; // 0.012467;  // RLWE key standard deviation
	private double stdevRLWE;            // 0; // 0.012467;     // RLWE ciphertexts standard deviation
	private double stdevRGSW;            // RGSW ciphertexts standard deviation
	private int bgBit;                   // Base bit gadget
	private int dg;                      // dimension gadget
	private double stdevBK;              // BK standard deviation
	private int generatedKeys;           // total number of LWE public keys per trustee
	private int encryptionKeys;          // number of LWE public keys per trustee added to plaintext bit

	@Temporal(TemporalType.TIMESTAMP)
	private Calendar voteStart;
	@Temporal(TemporalType.TIMESTAMP)
	private Calendar voteEnd;


	/**
	 * Default constructor.
	 */
	public VoteParams() {
		this.id = 0;
		this.vote = null;

		this.trustees = 2;
		this.bulletinBoards = 2;
		this.counterLength = 3;

		this.k = 1;

		this.ksStdev = 2.44e-5;
		this.bkStdev = 3.29e-10;
		this.maxStdev = 0.0078125;

		this.sn = 500;
		this.hLWE = 0;
		this.stdevLWE = 0.0078125;
		this.bksBit = 2;
		this.dks = 8;
		this.stdevKS = 2.44e-5;
		this.ln = 1024;
		this.hRLWE = 0;
		this.stdevRLWEkey = 3.29e-10;
		this.stdevRLWE = 3.29e-10;
		this.stdevRGSW = 3.29e-10;
		this.bgBit = 4;
		this.dg = 8;
		this.stdevBK = 3.29e-10;
		this.generatedKeys = 8192;
		this.encryptionKeys = 2;

		this.voteStart = null;
		this.voteEnd = null;
	}

	/**
	 * Default constructor with additional information.
	 *
	 * @param vote           {@link VoteExecution} the params belong to
	 * @param trustees       number fo Trustees
	 * @param bulletinBoards number of Bulletin Boards
	 * @param counterLength  number of choices
	 * @param voteStart      start date of the vote
	 * @param voteEnd        end date of the vote
	 */
	public VoteParams(VoteExecution vote, int trustees, int bulletinBoards, int counterLength, Calendar voteStart, Calendar voteEnd) {
		this.id = 0;
		this.vote = vote;

		this.trustees = trustees;
		this.bulletinBoards = bulletinBoards;
		this.counterLength = counterLength;

		this.k = 1;

		this.ksStdev = 2.44e-5;
		this.bkStdev = 3.29e-10;
		this.maxStdev = 0.0078125;

		this.sn = 500;
		this.hLWE = 0;
		this.stdevLWE = 0.0078125;
		this.bksBit = 2;
		this.dks = 8;
		this.stdevKS = 2.44e-5;
		this.ln = 1024;
		this.hRLWE = 0;
		this.stdevRLWEkey = 3.29e-10;
		this.stdevRLWE = 3.29e-10;
		this.stdevRGSW = 3.29e-10;
		this.bgBit = 4;
		this.dg = 8;
		this.stdevBK = 3.29e-10;
		this.generatedKeys = 8192;
		this.encryptionKeys = 2;

		this.voteStart = voteStart;
		this.voteEnd = voteEnd;
	}


	/**
	 * Constructor with all information.
	 *
	 * @param id             id of the params (can mostly be assigned to zero and will be assigned by JPA)
	 * @param vote           {@link VoteExecution} the params belong to
	 * @param trustees       number fo Trustees
	 * @param bulletinBoards number of Bulletin Boards
	 * @param counterLength  number of choices
	 * @param k              k
	 * @param ksStdev        standard deviation
	 * @param bkStdev        standard deviation
	 * @param maxStdev       max standard deviation for a 1/4 msg space
	 * @param sn             LWE modulus
	 * @param hLWE           HW secret key LWE --> not used
	 * @param stdevLWE       LWE ciphertexts standard deviation
	 * @param bksBit         Base bit key switching
	 * @param dks            dimension key switching
	 * @param stdevKS        KS key standard deviation
	 * @param ln             RLWE,RGSW modulus
	 * @param hRLWE          HW secret key RLWE,RGSW --> not used
	 * @param stdevRLWEkey   0; // 0.012467;  // RLWE key standard deviation
	 * @param stdevRLWE      0; // 0.012467;     // RLWE ciphertexts standard deviation
	 * @param stdevRGSW      RGSW ciphertexts standard deviation
	 * @param bgBit          Base bit gadget
	 * @param dg             dimension gadget
	 * @param stdevBK        BK standard deviation
	 * @param generatedKeys  total number of LWE public keys per trustee
	 * @param encryptionKeys number of LWE public keys per trustee added to plaintext bit
	 * @param voteStart      start date of the vote
	 * @param voteEnd        end date of the vote
	 */
	public VoteParams(long id, VoteExecution vote, int trustees, int bulletinBoards, int counterLength, int k, double ksStdev, double bkStdev, double maxStdev, int sn, int hLWE, double stdevLWE, long bksBit, int dks, double stdevKS,
					  int ln, int hRLWE, double stdevRLWEkey, double stdevRLWE, double stdevRGSW, int bgBit, int dg, double stdevBK, int generatedKeys, int encryptionKeys, Calendar voteStart, Calendar voteEnd) {
		this.id = id;
		this.vote = vote;

		if (trustees <= 0) {
			throw new IllegalArgumentException("number of trustees must be positive");
		}
		this.trustees = trustees;
		if (bulletinBoards <= 0) {
			throw new IllegalArgumentException("number of bulletinBoards must be positive");
		}
		this.bulletinBoards = bulletinBoards;
		if (counterLength <= 0) {
			throw new IllegalArgumentException("counterLength must be a positive number");
		}
		this.counterLength = counterLength;
		this.k = k;
		this.ksStdev = ksStdev;
		this.bkStdev = bkStdev;
		this.maxStdev = maxStdev;
		if (sn <= 0) {
			throw new IllegalArgumentException("sn must be a positive number");
		}
		this.sn = sn;
		this.hLWE = hLWE;
		this.stdevLWE = stdevLWE;
		this.bksBit = bksBit;
		this.dks = dks;
		this.stdevKS = stdevKS;
		if (ln <= 0) {
			throw new IllegalArgumentException("ln must be a positive number");
		}
		this.ln = ln;
		this.hRLWE = hRLWE;
		this.stdevRLWEkey = stdevRLWEkey;
		this.stdevRLWE = stdevRLWE;
		this.stdevRGSW = stdevRGSW;
		this.bgBit = bgBit;
		this.dg = dg;
		this.stdevBK = stdevBK;
		if (generatedKeys <= 0) {
			throw new IllegalArgumentException("number of generatedKeys must be positive");
		}
		this.generatedKeys = generatedKeys;
		if (encryptionKeys <= 0) {
			throw new IllegalArgumentException("number of encryptionKeys must be positive");
		}
		this.encryptionKeys = encryptionKeys;

		this.voteStart = voteStart;
		this.voteEnd = voteEnd;
	}

	public VoteParams(VoteParams p, VoteExecution voteParams) {
		this(0, voteParams, p.getTrustees(), p.bulletinBoards, p.counterLength, p.k, p.ksStdev, p.bkStdev, p.maxStdev, p.sn, p.hLWE, p.stdevLWE, p.bksBit, p.dks, p.stdevKS, p.ln, p.hRLWE, p.stdevRLWEkey, p.stdevRLWE, p.stdevRGSW, p.bgBit, p.dg, p.stdevBK, p.generatedKeys, p.encryptionKeys, p.voteStart, p.voteEnd);
	}

	@JsonIgnore
	public long getId() {
		return id;
	}

	@JsonIgnore
	public VoteExecution getVote() {
		return vote;
	}

	public int getTrustees() {
		return trustees;
	}

	public void setTrustees(int trustees) {
		this.failOnStarted();
		this.trustees = trustees;
	}

	public int getBulletinBoards() {
		return bulletinBoards;
	}

	public void setBulletinBoards(int bulletinBoards) {
		this.failOnStarted();
		this.bulletinBoards = bulletinBoards;
	}

	public int getCounterLength() {
		return counterLength;
	}

	public void setCounterLength(int counterLength) {
		this.failOnStarted();
		this.counterLength = counterLength;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.failOnStarted();
		this.k = k;
	}

	public double getKsStdev() {
		return ksStdev;
	}

	public void setKsStdev(double ksStdev) {
		this.failOnStarted();
		this.ksStdev = ksStdev;
	}

	public double getBkStdev() {
		return bkStdev;
	}

	public void setBkStdev(double bkStdev) {
		this.failOnStarted();
		this.bkStdev = bkStdev;
	}

	public double getMaxStdev() {
		return maxStdev;
	}

	public void setMaxStdev(double maxStdev) {
		this.failOnStarted();
		this.maxStdev = maxStdev;
	}

	public int getSn() {
		return sn;
	}

	public void setSn(int sn) {
		this.failOnStarted();
		this.sn = sn;
	}

	public int gethLWE() {
		return hLWE;
	}

	public void sethLWE(int hLWE) {
		this.failOnStarted();
		this.hLWE = hLWE;
	}

	public double getStdevLWE() {
		return stdevLWE;
	}

	public void setStdevLWE(double stdevLWE) {
		this.failOnStarted();
		this.stdevLWE = stdevLWE;
	}

	public long getBksBit() {
		return bksBit;
	}

	public void setBksBit(long bksBit) {
		this.failOnStarted();
		this.bksBit = bksBit;
	}

	public int getDks() {
		return dks;
	}

	public void setDks(int dks) {
		this.failOnStarted();
		this.dks = dks;
	}

	public double getStdevKS() {
		return stdevKS;
	}

	public void setStdevKS(double stdevKS) {
		this.failOnStarted();
		this.stdevKS = stdevKS;
	}

	public int getLn() {
		return ln;
	}

	public void setLn(int ln) {
		this.failOnStarted();
		this.ln = ln;
	}

	public int gethRLWE() {
		return hRLWE;
	}

	public void sethRLWE(int hRLWE) {
		this.failOnStarted();
		this.hRLWE = hRLWE;
	}

	public double getStdevRLWEkey() {
		return stdevRLWEkey;
	}

	public void setStdevRLWEkey(double stdevRLWEkey) {
		this.failOnStarted();
		this.stdevRLWEkey = stdevRLWEkey;
	}

	public double getStdevRLWE() {
		return stdevRLWE;
	}

	public void setStdevRLWE(double stdevRLWE) {
		this.failOnStarted();
		this.stdevRLWE = stdevRLWE;
	}

	public double getStdevRGSW() {
		return stdevRGSW;
	}

	public void setStdevRGSW(double stdevRGSW) {
		this.failOnStarted();
		this.stdevRGSW = stdevRGSW;
	}

	public int getBgBit() {
		return bgBit;
	}

	public void setBgBit(int bgBit) {
		this.failOnStarted();
		this.bgBit = bgBit;
	}

	public int getDg() {
		return dg;
	}

	public void setDg(int dg) {
		this.failOnStarted();
		this.dg = dg;
	}

	public double getStdevBK() {
		return stdevBK;
	}

	public void setStdevBK(double stdevBK) {
		this.failOnStarted();
		this.stdevBK = stdevBK;
	}

	public int getGeneratedKeys() {
		return generatedKeys;
	}

	public void setGeneratedKeys(int generatedKeys) {
		this.failOnStarted();
		this.generatedKeys = generatedKeys;
	}

	public int getEncryptionKeys() {
		return encryptionKeys;
	}

	public void setEncryptionKeys(int encryptionKeys) {
		this.failOnStarted();
		this.encryptionKeys = encryptionKeys;
	}

	public Calendar getVoteStart() {
		return voteStart;
	}

	@JsonIgnore
	public String getVoteStartText() {
		if (this.voteStart != null) {
			return new SimpleDateFormat("HH:mm dd-MM-yyyy").format(this.voteStart.getTime());
		}
		return "";
	}

	public void setVoteStart(Calendar voteStart) {
		this.failOnStarted();
		this.voteStart = voteStart;
	}

	public Calendar getVoteEnd() {
		return voteEnd;
	}

	@JsonIgnore
	public String getVoteEndText() {
		if (this.voteEnd != null) {
			return new SimpleDateFormat("HH:mm dd-MM-yyyy").format(this.voteEnd.getTime());
		}
		return "";
	}

	public void setVoteEnd(Calendar voteEnd) {
		this.voteEnd = voteEnd;
	}

	@JsonIgnore
	public double getStdevLwePK() {
		return stdevLWE / Math.sqrt(trustees * encryptionKeys);
	}

	/**
	 * Updates the params keeping the assigned id and {@link VoteExecution}.
	 *
	 * @param other new Params
	 */
	public void update(VoteParams other) {
		this.failOnStarted();

		this.trustees = other.trustees;
		this.bulletinBoards = other.bulletinBoards;

		this.k = other.k;

		this.ksStdev = other.ksStdev;
		this.bkStdev = other.bkStdev;
		this.maxStdev = other.maxStdev;

		this.sn = other.sn;
		this.hLWE = other.hLWE;
		this.stdevLWE = other.stdevLWE;
		this.bksBit = other.bksBit;
		this.dks = other.dks;
		this.stdevKS = other.stdevKS;
		this.ln = other.ln;
		this.hRLWE = other.hRLWE;
		this.stdevRLWEkey = other.stdevRLWEkey;
		this.stdevRLWE = other.stdevRLWE;
		this.stdevRGSW = other.stdevRGSW;
		this.bgBit = other.bgBit;
		this.dg = other.dg;
		this.stdevBK = other.stdevBK;
		this.generatedKeys = other.generatedKeys;
		this.encryptionKeys = other.encryptionKeys;

		this.voteStart = other.voteStart;
		this.voteEnd = other.voteEnd;
	}

	private void failOnStarted() {
		if (this.vote != null && this.vote.isStarted()) {
			throw new IllegalStateException("Cant change vote params after start");
		}
	}
}
