package ch.hsr.ba.coordinator.data.model.authentication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Trustee")
public class Trustee extends PermissionComponent {
	public Trustee() {
		super();
	}

	public Trustee(String apiKey, String hostname, int port) {
		super(0, apiKey, hostname, port);
	}
}