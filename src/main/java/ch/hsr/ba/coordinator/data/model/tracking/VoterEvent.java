package ch.hsr.ba.coordinator.data.model.tracking;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Calendar;

@Entity
@DiscriminatorValue("Voter")
public class VoterEvent extends VoteEvent {
	public VoterEvent() {
		super();
	}

	public VoterEvent(long id, Vote vote, Calendar time, String type) {
		super(id, vote, time, type);
	}
}
