package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BulletinBoardRepository extends JpaRepository<BulletinBoard, Long> {
	@Query("SELECT bb FROM BulletinBoard bb WHERE bb.apiKey = ?1")
	Optional<BulletinBoard> findByApiKey(String hash);

	@Query("SELECT bb FROM BulletinBoard bb JOIN VoteBulletinBoard vbb ON (bb.id = vbb.bulletinBoard.id) WHERE vbb.voteExecution = ?1")
	List<BulletinBoard> findByVoteExecution(VoteExecution voteExecution);
}
