package ch.hsr.ba.coordinator.data.model.vote;

import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "vote_bulletin_board", uniqueConstraints = {@UniqueConstraint(columnNames = {"bulletin_board_id", "vote_execution_id"})})
public class VoteBulletinBoard {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@ManyToOne
	private final BulletinBoard bulletinBoard;
	@ManyToOne
	private final VoteExecution voteExecution;

	private boolean ready;

	@OneToOne(mappedBy = "bulletinBoard", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private BulletinBoardResult bulletinBoardResult;

	public VoteBulletinBoard() {
		this.id = 0;
		this.bulletinBoard = null;
		this.voteExecution = null;
		this.ready = false;
	}

	public VoteBulletinBoard(long id, BulletinBoard bulletinBoard, VoteExecution voteExecution) {
		this.id = id;
		this.bulletinBoard = bulletinBoard;
		this.voteExecution = voteExecution;
		this.ready = false;
	}

	public long getId() {
		return id;
	}

	public BulletinBoard getBulletinBoard() {
		return bulletinBoard;
	}

	@JsonIgnore
	public VoteExecution getVoteExecution() {
		return voteExecution;
	}

	@JsonIgnore
	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	@JsonIgnore
	public BulletinBoardResult getBulletinBoardResult() {
		return bulletinBoardResult;
	}

	public void setBulletinBoardResult(BulletinBoardResult bulletinBoardResult) {
		this.bulletinBoardResult = bulletinBoardResult;
	}
}
