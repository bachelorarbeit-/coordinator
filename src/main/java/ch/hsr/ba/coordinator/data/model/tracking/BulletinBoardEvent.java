package ch.hsr.ba.coordinator.data.model.tracking;

import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Calendar;

@Entity
@DiscriminatorValue("BulletinBoard")
public class BulletinBoardEvent extends VoteEvent {
	@ManyToOne
	private final BulletinBoard bulletinBoard;

	public BulletinBoardEvent() {
		this.bulletinBoard = null;
	}

	public BulletinBoardEvent(long id, Vote vote, BulletinBoard bulletinBoard, Calendar time, String type) {
		super(id, vote, time, type);
		this.bulletinBoard = bulletinBoard;
	}

	public BulletinBoard getBulletinBoard() {
		return bulletinBoard;
	}
}
