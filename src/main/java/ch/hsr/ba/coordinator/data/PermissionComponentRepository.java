package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.PermissionComponent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionComponentRepository extends JpaRepository<PermissionComponent, Long> {
	@Query("SELECT pc FROM PermissionComponent pc WHERE pc.apiKey = ?1")
	PermissionComponent hasValidKey(String key);
}
