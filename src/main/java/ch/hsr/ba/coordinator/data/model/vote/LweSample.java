package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "lwe_sample")
public class LweSample {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@ManyToOne(cascade = CascadeType.PERSIST)
	private final VoteTrustee voteTrustee;

	@Lob
	private final Integer[] phaseShift;
	private final int phase;

	public LweSample() {
		this.id = 0;
		this.voteTrustee = null;
		this.phaseShift = new Integer[500];
		this.phase = 0;
	}

	public LweSample(long id, VoteTrustee voteTrustee, Integer[] phaseShift, int phase) {
		this.id = id;
		this.voteTrustee = voteTrustee;
		this.phaseShift = phaseShift;
		this.phase = phase;
	}

	public long getId() {
		return id;
	}

	@JsonIgnore
	public VoteTrustee getVoteTrustee() {
		return voteTrustee;
	}

	public Integer[] getPhaseShift() {
		return phaseShift;
	}

	public int getPhase() {
		return phase;
	}
}
