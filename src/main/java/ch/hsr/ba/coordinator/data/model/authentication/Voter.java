package ch.hsr.ba.coordinator.data.model.authentication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Voter")
public class Voter extends PermissionComponent {
	public Voter() {
		super();
	}

	public Voter(String apiKey) {
		super(0, apiKey, null, 0);
	}
}
