package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface VoterRepository extends JpaRepository<Voter, Long> {
	@Query("SELECT vt FROM Voter vt WHERE vt.apiKey = ?1")
	Optional<Voter> findByApiKey(String hash);
}
