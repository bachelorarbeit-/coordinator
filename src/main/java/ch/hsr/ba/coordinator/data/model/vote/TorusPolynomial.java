package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.security.SecureRandom;

@Entity
@Table(name = "torus_polynomial")
public class TorusPolynomial {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@ManyToOne
	private final VoteExecution voteExecution;

	private final int size;

	@Lob
	private final Integer[] coefsT;

	public TorusPolynomial() {
		this.id = 0;
		this.voteExecution = null;
		this.size = 1024;
		this.coefsT = new Integer[1024];
	}

	public TorusPolynomial(VoteExecution voteExecution) {
		this.id = 0;
		this.voteExecution = voteExecution;
		this.size = voteExecution.getVoteParams().getLn();
		this.coefsT = new Integer[this.size];

		SecureRandom random = new SecureRandom();
		for (int i = 0; i < this.size; i++) {
			this.coefsT[i] = random.nextInt();
		}
	}

	public TorusPolynomial(long id, VoteExecution voteExecution, int size, Integer[] coefsT) {
		this.id = id;
		this.voteExecution = voteExecution;
		this.size = size;
		this.coefsT = coefsT;
	}

	@JsonIgnore
	public long getId() {
		return id;
	}

	@JsonIgnore
	public VoteExecution getVoteExecution() {
		return voteExecution;
	}

	public int getSize() {
		return size;
	}

	public Integer[] getCoefsT() {
		return coefsT;
	}
}
