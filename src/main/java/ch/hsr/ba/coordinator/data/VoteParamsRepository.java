package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.vote.VoteParams;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteParamsRepository extends JpaRepository<VoteParams, Long> {
}
