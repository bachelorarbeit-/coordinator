package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface VoteExecutionRepository extends JpaRepository<VoteExecution, Long> {
	@Query("SELECT ve FROM VoteExecution ve LEFT JOIN VoteParams vp ON (ve.id=vp.vote) WHERE ve.start = 1 AND vp.voteStart < CURRENT_TIMESTAMP() AND vp.voteEnd > CURRENT_TIMESTAMP()")
	List<VoteExecution> findAllActive();

	@Query("SELECT ve FROM VoteExecution ve LEFT JOIN VoteParams vp ON (ve.id=vp.vote) WHERE ve.start = 1 AND vp.voteStart > CURRENT_TIMESTAMP()")
	List<VoteExecution> findAllInPreparation();

	@Query("SELECT ve FROM VoteExecution ve LEFT JOIN VoteParams vp ON (ve.id=vp.vote) WHERE ve.start = 1 AND vp.voteEnd < CURRENT_TIMESTAMP()")
	List<VoteExecution> findAllDone();

	@Query("SELECT ve FROM VoteExecution ve INNER JOIN Vote vo ON (vo.voteExecution = ve) WHERE vo.voter=?1")
	List<VoteExecution> findAllParticipations(Voter voter);
}
