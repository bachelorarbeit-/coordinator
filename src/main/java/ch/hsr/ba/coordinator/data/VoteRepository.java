package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.data.model.tracking.Vote;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {
	@Query("SELECT v FROM Vote v WHERE v.voter = ?1")
	List<Vote> findByVoter(Voter voter);

	@Query("SELECT v FROM Vote v WHERE v.voter = ?1 AND v.voteExecution = ?2")
	Optional<Vote> findByVoterAndVote(Voter voter, VoteExecution voteExecution);

}
