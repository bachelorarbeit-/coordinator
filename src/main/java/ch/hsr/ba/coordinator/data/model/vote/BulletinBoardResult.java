package ch.hsr.ba.coordinator.data.model.vote;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bulletin_board_result")
public class BulletinBoardResult {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;
	@OneToOne
	private final VoteBulletinBoard bulletinBoard;
	@OneToMany(cascade = CascadeType.ALL)
	private final List<OptionCounter> samples;

	public BulletinBoardResult() {
		this.id = 0;
		this.bulletinBoard = null;
		this.samples = new ArrayList<>();
	}

	public BulletinBoardResult(long id, VoteBulletinBoard bulletinBoard, List<OptionCounter> samples) {
		this.id = id;
		this.bulletinBoard = bulletinBoard;
		this.samples = samples;
	}

	public long getId() {
		return id;
	}

	public VoteBulletinBoard getBulletinBoard() {
		return bulletinBoard;
	}

	public List<OptionCounter> getSamples() {
		return samples;
	}
}
