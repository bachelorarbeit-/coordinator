package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TrusteeRepository extends JpaRepository<Trustee, Long> {
	@Query("SELECT tr FROM Trustee tr WHERE tr.apiKey = ?1")
	Optional<Trustee> findByApiKey(String hash);
}
