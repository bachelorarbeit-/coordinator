package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vote_choices")
public class VoteChoices {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;
	@Column(name = "option_order")
	private final int order;
	private final String option;

	@ManyToOne
	private final VoteExecution vote;

	public VoteChoices() {
		this.id = 0;
		this.order = 0;
		this.option = null;
		this.vote = null;
	}

	public VoteChoices(long id, int order, String option, VoteExecution vote) {
		this.id = id;
		this.order = order;
		this.option = option;
		this.vote = vote;
	}

	public long getId() {
		return id;
	}

	public int getOrder() {
		return order;
	}

	public String getOption() {
		return option;
	}

	@JsonIgnore
	public VoteExecution getVote() {
		return vote;
	}
}
