package ch.hsr.ba.coordinator.data.model.tracking;

import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "vote", uniqueConstraints = {@UniqueConstraint(columnNames = {"voter_id", "vote_execution_id"})})
public class Vote {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@ManyToOne
	private final Voter voter;
	@ManyToOne
	private final VoteExecution voteExecution;

	public Vote() {
		this.id = 0;
		this.voter = null;
		this.voteExecution = null;
	}

	public Vote(long id, Voter voter, VoteExecution voteExecution) {
		this.id = id;
		this.voter = voter;
		this.voteExecution = voteExecution;
	}

	public long getId() {
		return id;
	}

	public Voter getVoter() {
		return voter;
	}

	public VoteExecution getVoteExecution() {
		return voteExecution;
	}
}
