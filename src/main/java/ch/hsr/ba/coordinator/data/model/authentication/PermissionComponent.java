package ch.hsr.ba.coordinator.data.model.authentication;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Calendar;

@Entity
@Table(name = "permission_component")
@DiscriminatorColumn(name = "ROLE")
public abstract class PermissionComponent {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@JsonIgnore
	private final String apiKey;

	private final String hostname;
	private final int port;

	private Calendar lastContact;
	private boolean running;

	public PermissionComponent() {
		this.id = 0;
		this.apiKey = null;
		this.hostname = null;
		this.port = 0;

	}

	public PermissionComponent(long id, String apiKey, String hostname, int port) {
		this.id = id;
		this.apiKey = apiKey;
		this.hostname = hostname;
		this.port = port;
	}

	public long getId() {
		return id;
	}

	@JsonIgnore
	public String getApiKey() {
		return apiKey;
	}

	public String getHostname() {
		return hostname;
	}

	public int getPort() {
		return port;
	}

	@JsonIgnore
	public Calendar getLastContact() {
		return lastContact;
	}

	public void setLastContact(Calendar lastContact) {
		this.lastContact = lastContact;
	}

	@JsonIgnore
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public void successfulContact() {
		this.lastContact = Calendar.getInstance();
		this.running = true;
	}

	public void failedContact() {
		this.running = false;
	}
}
