package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.tracking.Vote;
import ch.hsr.ba.coordinator.data.model.tracking.VoteEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TrackingRepository extends JpaRepository<VoteEvent, Long> {
	@Query("SELECT ve FROM VoteEvent ve WHERE ve.vote = ?1")
	List<VoteEvent> findAllByVote(Vote vote);
}
