package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "vote_execution")
public class VoteExecution {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	private final String question;

	@OneToMany(mappedBy = "vote", cascade = CascadeType.ALL)
	private final List<VoteChoices> choices;

	@OneToOne(mappedBy = "vote", cascade = CascadeType.ALL)
	private VoteParams voteParams;

	@OneToMany(cascade = CascadeType.ALL)
	private List<VoteTrustee> trustees;

	@OneToMany(cascade = CascadeType.ALL)
	private List<VoteBulletinBoard> bulletinBoards;

	@OneToMany(mappedBy = "voteExecution", cascade = CascadeType.ALL)
	private List<TorusPolynomial> rLwePubKey;

	private boolean start;
	private boolean done;

	/**
	 * Default constructor.
	 */
	public VoteExecution() {
		this.id = 0;
		this.question = null;
		this.choices = new ArrayList<>();
		this.voteParams = null;
		this.trustees = new ArrayList<>();
		this.bulletinBoards = new ArrayList<>();
		this.start = false;
		this.done = false;
	}

	/**
	 * default constructor foe new votes.
	 *
	 * @param id       VoteExecution id
	 * @param question question to be voted up on
	 * @param choices  options to vote
	 */
	public VoteExecution(long id, String question, List<VoteChoices> choices) {
		this.id = id;
		this.question = question;
		this.choices = choices;
		this.voteParams = null;
		this.trustees = new ArrayList<>();
		this.bulletinBoards = new ArrayList<>();
		this.start = false;
		this.done = false;
	}

	/**
	 * constructor for existing votes with assigned Trustees and BBs.
	 *
	 * @param id             VoteExecution id
	 * @param question       question to be voted up on
	 * @param choices        options to vote
	 * @param voteParams     parameters for the vote
	 * @param trustees       list of assigned trustees
	 * @param bulletinBoards list of assigned bulletin boards
	 */
	public VoteExecution(long id, String question, List<VoteChoices> choices, VoteParams voteParams, List<VoteTrustee> trustees, List<VoteBulletinBoard> bulletinBoards) {
		this.id = id;
		this.question = question;
		this.choices = choices;
		this.voteParams = voteParams;
		this.trustees = trustees;
		this.bulletinBoards = bulletinBoards;
		this.start = false;
		this.done = false;
	}

	public long getId() {
		return id;
	}

	public String getQuestion() {
		return question;
	}

	public List<VoteChoices> getChoices() {
		if (start) {
			return Collections.unmodifiableList(choices);
		}
		return choices;
	}

	public VoteParams getVoteParams() {
		return voteParams;
	}

	public void setVoteParams(VoteParams voteParams) {
		this.failIfStarted();
		this.voteParams = voteParams;
	}

	private void failIfStarted() {
		if (this.start) {
			throw new IllegalStateException("Cant change vote execution after start");
		}
	}

	public List<VoteTrustee> getTrustees() {
		if (this.isLocked()) {
			return Collections.unmodifiableList(trustees);
		}
		return trustees;
	}

	public List<VoteBulletinBoard> getBulletinBoards() {
		if (this.isLocked()) {
			return Collections.unmodifiableList(bulletinBoards);
		}
		return bulletinBoards;
	}

	public List<TorusPolynomial> getrLwePubKey() {
		return rLwePubKey;
	}

	/**
	 * Generates RLWEKey using Secure Random.
	 */
	public void generateRLWEKey() {
		if (this.start && this.rLwePubKey.size() > 0) {
			throw new IllegalStateException("Cant change vote execution after start");
		}

		int dg = this.getVoteParams().getDg();
		List<TorusPolynomial> rlweKeys = new ArrayList<>();
		for (int i = 0; i < dg; i++) {
			rlweKeys.add(new TorusPolynomial(this));
		}
		this.rLwePubKey = rlweKeys;
	}

	public void setrLwePubKey(List<TorusPolynomial> rLwePubKey) {
		this.failIfStarted();
		this.rLwePubKey = rLwePubKey;
	}

	public void start() {
		if (this.voteParams == null) {
			throw new IllegalStateException("Cannot start without params");
		}
		this.start = true;
	}

	@JsonIgnore
	public boolean isStarted() {
		return start;
	}

	@JsonIgnore
	public boolean isLocked() {
		return start && this.voteParams != null && this.trustees.size() == this.voteParams.getTrustees() && this.bulletinBoards.size() == this.voteParams.getBulletinBoards();
	}

	/**
	 * Checks weather all components have reached the readystate.
	 *
	 * @return true if ready otherwise false
	 */
	@JsonIgnore
	public boolean isReady() {
		if (!isLocked()) {
			return false;
		}
		for (VoteTrustee voteTrustee : this.getTrustees()) {
			if (!voteTrustee.isReady()) {
				return false;
			}
		}
		for (VoteBulletinBoard board : this.getBulletinBoards()) {
			if (!board.isReady()) {
				return false;
			}
		}
		return true;
	}

	@JsonIgnore
	public boolean isOpen() {
		Calendar now = Calendar.getInstance();
		return isLocked() && this.getVoteParams().getVoteStart().before(now) && this.getVoteParams().getVoteEnd().after(now) && !this.done;
	}

	@JsonIgnore
	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}
}
