package ch.hsr.ba.coordinator.data.model.vote;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "option_counter")
public class OptionCounter {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@ManyToOne
	private final VoteChoices choices;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private final List<MkLweSample> mkLweSamples;

	private boolean calculated;
	private int result;

	public OptionCounter() {
		this.id = 0;
		this.choices = null;
		this.mkLweSamples = new ArrayList<>();
		this.calculated = false;
		this.result = 0;
	}

	public OptionCounter(long id, VoteChoices choices, List<MkLweSample> mkLweSamples, boolean calculated, int result) {
		this.id = id;
		this.choices = choices;
		this.mkLweSamples = mkLweSamples;
		this.calculated = calculated;
		this.result = result;
	}

	public long getId() {
		return id;
	}

	public List<MkLweSample> getMkLweSamples() {
		return mkLweSamples;
	}

	@JsonIgnore
	public VoteChoices getChoices() {
		return choices;
	}

	@JsonIgnore
	public boolean isCalculated() {
		return this.calculated;
	}

	@JsonIgnore
	public int getResult() {
		return this.result;
	}

	@JsonIgnore
	public void calculateResult() {
		int result = 0;
		for (int i = 0; i < mkLweSamples.size(); i++) {
			result += (mkLweSamples.get(i).getResult() ? 1 : 0) << i;
		}
		this.calculated = true;
		this.result = result;
	}

	@JsonIgnore
	public String getResultString() {
		if(!this.calculated) {
			try {
				this.calculateResult();
				return Integer.toString(this.result);
			} catch (IllegalStateException e) {
				return "";
			}
		}
		return Integer.toString(this.result);
	}
}
