package ch.hsr.ba.coordinator.data.model.authentication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("BulletinBoard")
public class BulletinBoard extends PermissionComponent {
	public BulletinBoard() {
		super();
	}

	public BulletinBoard(String apiKey, String hostname, int port) {
		super(0, apiKey, hostname, port);
	}
}
