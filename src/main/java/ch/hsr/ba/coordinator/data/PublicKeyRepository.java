package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteTrustee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PublicKeyRepository extends JpaRepository<LweSample, Long> {
	@Query("SELECT s FROM LweSample s WHERE s.voteTrustee = ?1")
	List<LweSample> findAllByVoteTrustee(VoteTrustee voteTrustee);
}
