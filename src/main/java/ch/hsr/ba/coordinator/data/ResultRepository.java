package ch.hsr.ba.coordinator.data;

import ch.hsr.ba.coordinator.data.model.vote.BulletinBoardResult;
import ch.hsr.ba.coordinator.data.model.vote.VoteBulletinBoard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ResultRepository extends JpaRepository<BulletinBoardResult, Long> {
	@Query("SELECT bbr FROM BulletinBoardResult bbr WHERE bbr.bulletinBoard = ?1")
	Optional<BulletinBoardResult> findByVoteBulletinBoard(VoteBulletinBoard board);
}
