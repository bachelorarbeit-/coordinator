package ch.hsr.ba.coordinator.data.model.vote;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "mk_lwe_sample")
public class MkLweSample {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private final long id;

	@Lob
	private final Integer[] phaseShift;
	private final int phase;

	@Lob
	private Integer[] decryption;
	@Lob
	private Boolean[] decrypted;

	/**
	 * default constructor of MkLweSample.
	 */
	public MkLweSample() {
		this.id = 0;
		this.phaseShift = new Integer[1];
		this.phase = 0;
		this.decryption = new Integer[1];
		Arrays.fill(this.decryption, 0);
		this.decrypted = new Boolean[1];
		Arrays.fill(this.decrypted, false);
	}


	/**
	 * non default constructor of MkLweSample.
	 *
	 * @param id         Id of the MkLweSample, can in most cases be set to 0 since the id will be assigned by JPA
	 * @param phaseShift Key components of the TFHE encryption (Called A in MK-TFHE library).
	 * @param phase      Value components of the TFHE encryption (Called B in MK-TFHE library).
	 * @param trustees   number of Trustees
	 */
	public MkLweSample(long id, Integer[] phaseShift, int phase, int trustees) {
		this.id = id;
		this.phaseShift = phaseShift;
		this.phase = phase;
		this.decryption = new Integer[trustees];
		Arrays.fill(this.decryption, 0);
		this.decrypted = new Boolean[trustees];
		Arrays.fill(this.decrypted, false);
	}

	public long getId() {
		return id;
	}

	public Integer[] getPhaseShift() {
		return phaseShift;
	}

	public int getPhase() {
		return phase;
	}

	public Integer[] getDecryption() {
		return decryption;
	}

	public void setDecryption(Integer[] decryption) {
		this.decryption = decryption;
	}

	public Boolean[] getDecrypted() {
		return decrypted;
	}

	/**
	 * Calculates the boolean value of the MkLweSample.
	 *
	 * @return boolean value of the MkLweSample
	 * @throws IllegalStateException if not all partial decryption values are present
	 */
	public boolean getResult() {
		int decB = phase;
		for (int i = 0; i < decrypted.length; i++) {
			if (!decrypted[i]) {
				throw new IllegalStateException("Decryption is not complete");
			} else {
				decB -= decryption[i];
			}
		}
		return decB > 0;
	}
}
