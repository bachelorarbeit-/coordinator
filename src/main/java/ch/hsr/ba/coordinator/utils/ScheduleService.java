package ch.hsr.ba.coordinator.utils;

import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.List;

@Service
public class ScheduleService {
	private static final Logger log = Logger.getLogger(ScheduleService.class);

	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	BulletinBoardRepository bulletinBoardRepository;

	@Scheduled(fixedRate = 10000)
	public void checkVoteComplete() {
		Calendar now = Calendar.getInstance();
		List<VoteExecution> voteExecutions = voteExecutionRepository.findAll();
		for (VoteExecution ve : voteExecutions) {
			if (!ve.isDone() && ve.getVoteParams().getVoteEnd().before(now)) {
				for (BulletinBoard bulletinBoard : bulletinBoardRepository.findByVoteExecution(ve)) {
					try (CloseableHttpClient client = HttpClients.createDefault()) {
						URI url = new URI("http", null, bulletinBoard.getHostname(), bulletinBoard.getPort(), "/votedone", null, null);
						HttpPost httpPost = new HttpPost(url);
						CloseableHttpResponse response = client.execute(httpPost);
					} catch (IOException | URISyntaxException e) {
						log.error(e.getLocalizedMessage(), e);
					}
				}
				ve.setDone(true);
				voteExecutionRepository.save(ve);
			}
		}
	}
}
