package ch.hsr.ba.coordinator.utils;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

public class PasswordUtil {
	/**
	 * Hashes a password to store it in a Database.
	 *
	 * @param password password to be hashed
	 * @param salt     salt used for the Hash
	 * @return hashed password
	 */
	public static String hash(final char[] password, final byte[] salt) {
		final int iterations = 1000;
		final int keyLength = 512;
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
			PBEKeySpec spec = new PBEKeySpec(password, salt, iterations, keyLength);
			SecretKey key = skf.generateSecret(spec);
			byte[] res = key.getEncoded();
			return Base64.getEncoder().withoutPadding().encodeToString(res);

		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new RuntimeException(e);
		}
	}

	public static byte[] generateSalt() {
		SecureRandom random = new SecureRandom();
		byte[] bytes = new byte[20];
		random.nextBytes(bytes);
		return bytes;
	}

	public static boolean verify(String hash, byte[] salt, final char[] password) {
		return PasswordUtil.hash(password, salt).equals(hash);
	}

	public static String generateApiKey(int length) {
		SecureRandom random = new SecureRandom();
		byte[] bytes = new byte[3 * (length / 4)];
		random.nextBytes(bytes);
		return Base64.getEncoder().withoutPadding().encodeToString(bytes);
	}
}
