package ch.hsr.ba.coordinator.configuration;

import ch.hsr.ba.coordinator.data.PermissionComponentRepository;
import ch.hsr.ba.coordinator.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class APIKeyAuthenticationManager implements AuthenticationManager {
	@Autowired
	private PermissionComponentRepository permissionComponentRepository;

	@Value("${auth.bearerPrefix}")
	private String bearerPrefix;
	@Value("${auth.salt}")
	private String authSalt;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		final String principal = (String) authentication.getPrincipal();
		if (!principal.startsWith(bearerPrefix + " ")) {
			throw new BadCredentialsException("Invalid API Key");
		}

		final String apiKey = principal.substring(bearerPrefix.length()).trim();
		if (permissionComponentRepository.hasValidKey(PasswordUtil.hash(apiKey.toCharArray(), Base64.getDecoder().decode(this.authSalt))) == null) {
			throw new BadCredentialsException("Invalid API Key");
		}

		authentication.setAuthenticated(true);
		return authentication;
	}
}
