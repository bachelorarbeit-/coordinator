package ch.hsr.ba.coordinator.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	private static final String[] AUTH_WHITELIST = {
			"/voter/**",
			"/resources/**",
			"/**.html",
			"/",
			"/test/**",
			"/trustee/register",
			"/bulletinBoard/register",
			"/clientapplication/**",
			"/results/**",
			"/applications/**",
			"/election/**",
			"/website/**",
	};

	@Autowired
	APIKeyAuthenticationManager apiKeyAuthenticationManager;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
				.withUser("admin").password("{noop}a47S3x5de2hvsBraD8GXY8K8")
				.roles("USER", "ADMIN");
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		final APIKeyAuthFilter filter = new APIKeyAuthFilter("Authorization");
		filter.setAuthenticationManager(apiKeyAuthenticationManager);

		httpSecurity.headers().frameOptions().disable().and().csrf().disable().exceptionHandling()
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
				.addFilter(filter).authorizeRequests()
				.antMatchers("/admin/**").authenticated().and().httpBasic().and().authorizeRequests()
				.antMatchers(AUTH_WHITELIST).permitAll().anyRequest().authenticated();

		// Disable Cache-Control for Conditional Requests
		httpSecurity.headers().cacheControl().disable();
	}
}
