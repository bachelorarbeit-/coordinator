package ch.hsr.ba.coordinator.controllers;

import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.TrusteeRepository;
import ch.hsr.ba.coordinator.data.VoterRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Controller;

import java.util.Base64;
import java.util.Optional;

@Controller
public class PermissionController {
	@Autowired
	VoterRepository voterRepository;
	@Autowired
	TrusteeRepository trusteeRepository;
	@Autowired
	BulletinBoardRepository bulletinBoardRepository;

	@Value("${auth.bearerPrefix}")
	private String bearerPrefix;
	@Value("${auth.salt}")
	private String authSalt;

	/**
	 * Checks if a voterId and an ApiKey belong together.
	 *
	 * @param voterId id of a Voter
	 * @param apiKey  ApiKey of a Voter
	 * @throws BadCredentialsException if id and ApiKey dont match
	 */
	public void checkPermissions(long voterId, String apiKey) {
		Optional<Voter> voter = voterRepository.findById(voterId);
		if (voter.isPresent()) {
			byte[] salt = Base64.getDecoder().decode(authSalt);
			apiKey = apiKey.substring(bearerPrefix.length()).trim();
			if (!PasswordUtil.verify(voter.get().getApiKey(), salt, apiKey.toCharArray())) {
				throw new BadCredentialsException("Invalid API Key");
			}
			return;
		}
		throw new BadCredentialsException("Invalid API Key");
	}

	/**
	 * Checks if a Trustee with a given ApiKey exists.
	 *
	 * @param apiKey ApiKey of a Trustee
	 * @return the Trustee to the ApiKey
	 * @throws BadCredentialsException if no Trustee with the ApiKey exists
	 */
	public Trustee getAuthorizedTrustee(String apiKey) {
		apiKey = apiKey.substring(bearerPrefix.length()).trim();
		Optional<Trustee> trustee = trusteeRepository.findByApiKey(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt()));
		if (trustee.isEmpty()) {
			throw new BadCredentialsException("Invalid API Key");
		}
		return trustee.get();
	}

	/**
	 * Checks if a BulletinBoard with a given ApiKey exists.
	 *
	 * @param apiKey ApiKey of a BulletinBoard
	 * @return the BulletinBoard to the ApiKey
	 * @throws BadCredentialsException if no BulletinBoard with the ApiKey exists
	 */
	public BulletinBoard getAuthorizedBulletinBoard(String apiKey) {
		apiKey = apiKey.substring(bearerPrefix.length()).trim();
		Optional<BulletinBoard> bulletinBoard = bulletinBoardRepository.findByApiKey(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt()));
		if (bulletinBoard.isEmpty()) {
			throw new BadCredentialsException("Invalid API Key");
		}
		return bulletinBoard.get();
	}

	private byte[] getSalt() {
		return Base64.getDecoder().decode(this.authSalt);
	}

	public Voter addVoter(String apiKey) {
		return voterRepository.save(new Voter(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt())));
	}

	public BulletinBoard addBulletinBoard(String apiKey, String hostname, int port) {
		return bulletinBoardRepository.save(new BulletinBoard(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt()), hostname, port));
	}

	public Trustee addTrustee(String apiKey, String hostname, int port) {
		return trusteeRepository.save(new Trustee(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt()), hostname, port));
	}

	/**
	 * Checks if a Voter with a given ApiKey exists.
	 *
	 * @param apiKey ApiKey of a Voter
	 * @return the Voter to the ApiKey
	 * @throws BadCredentialsException if no Voter with the ApiKey exists
	 */
	public Voter getAuthorizedVoter(String apiKey) {
		apiKey = apiKey.substring(bearerPrefix.length()).trim();
		Optional<Voter> voter = voterRepository.findByApiKey(PasswordUtil.hash(apiKey.toCharArray(), this.getSalt()));
		if (voter.isEmpty()) {
			throw new BadCredentialsException("Invalid API Key");
		}
		return voter.get();
	}
}
