package ch.hsr.ba.coordinator.controllers.web;

import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.stream.Collectors;


@Controller
@RequestMapping("/election")
public class ElectionController {

	@Autowired
	VoteExecutionRepository voteExecutionRepository;

	@RequestMapping("")
	public String overview(Model model) {
		model.addAttribute("voteExecutionsStarted", voteExecutionRepository.findAllInPreparation());
		model.addAttribute("voteExecutionsOpen", voteExecutionRepository.findAllActive().stream()
				.filter(VoteExecution::isReady).collect(Collectors.toList()));
		return "website/views/election.html";
	}
}