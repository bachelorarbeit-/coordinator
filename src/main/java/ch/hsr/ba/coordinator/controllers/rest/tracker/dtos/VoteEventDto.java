package ch.hsr.ba.coordinator.controllers.rest.tracker.dtos;

public class VoteEventDto {
	private final long voteId;
	private final String type;

	public VoteEventDto(long voteId, String type) {
		this.voteId = voteId;
		this.type = type;
	}

	public long getVoteId() {
		return voteId;
	}

	public String getType() {
		return type;
	}
}
