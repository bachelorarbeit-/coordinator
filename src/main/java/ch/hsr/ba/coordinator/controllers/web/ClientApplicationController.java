package ch.hsr.ba.coordinator.controllers.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/clientapplication")
public class ClientApplicationController {

	@RequestMapping("")
	public String overview(Model model) {
		return "website/views/clientApplication.html";
	}
}