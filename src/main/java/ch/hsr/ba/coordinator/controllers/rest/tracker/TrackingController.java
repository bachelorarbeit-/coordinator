package ch.hsr.ba.coordinator.controllers.rest.tracker;

import ch.hsr.ba.coordinator.controllers.PermissionController;
import ch.hsr.ba.coordinator.controllers.rest.tracker.dtos.BulletinBoardEventDto;
import ch.hsr.ba.coordinator.controllers.rest.tracker.dtos.EventResponseDto;
import ch.hsr.ba.coordinator.controllers.rest.tracker.dtos.VoteEventDto;
import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.TrackingRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.VoteRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.data.model.tracking.BulletinBoardEvent;
import ch.hsr.ba.coordinator.data.model.tracking.Vote;
import ch.hsr.ba.coordinator.data.model.tracking.VoteEvent;
import ch.hsr.ba.coordinator.data.model.tracking.VoterEvent;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;

@RestController
public class TrackingController {
	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	TrackingRepository trackingRepository;
	@Autowired
	VoteRepository voteRepository;
	@Autowired
	BulletinBoardRepository bulletinBoardRepository;

	@Autowired
	PermissionController permissions;

	@Value("${tracker.voter.events}")
	private String voterEvents;
	@Value("${tracker.bulletinBoard.events}")
	private String bulletinBoardEvents;

	/**
	 * Responds with al list of all events that have occurred for a given vote.
	 *
	 * @param voteId id of a voteExecution
	 * @param apiKey valid ApiKey of a Voter
	 * @return List of events for a vote
	 */
	@GetMapping("/voter/{voteId}/getState")
	public List<EventResponseDto> getState(@PathVariable(value = "voteId") long voteId, @RequestHeader("Authorization") String apiKey) {
		Voter voter = permissions.getAuthorizedVoter(apiKey);
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(voteId);
		if(voteExecution.isPresent()) {
			VoteExecution ve  = voteExecution.get();
			Optional<Vote> vote = voteRepository.findByVoterAndVote(voter, ve);
			if (vote.isPresent()) {
				List<VoteEvent> voteEvents = trackingRepository.findAllByVote(vote.get());
				return getEventResponseDtos(voteEvents);
			}
		}
		return new ArrayList<>();
	}

	/**
	 * Tracks an event that has occurred on a Voter.
	 *
	 * @param eventDto event related information
	 * @param apiKey   valid ApiKey of a Voter
	 */
	@PostMapping("/voter/trackVote")
	public void packageReceived(@RequestBody VoteEventDto eventDto, @RequestHeader("Authorization") String apiKey) {
		Optional<Vote> vote = voteRepository.findById(eventDto.getVoteId());
		permissions.checkPermissions(vote.get().getVoter().getId(), apiKey);
		if (vote.isPresent() && voterEvents.contains(eventDto.getType())) {
			trackingRepository.save(new VoterEvent(0, vote.get(), Calendar.getInstance(), eventDto.getType()));
		}
	}

	/**
	 * Tracks an event that has occurred on a Bulletin Board.
	 *
	 * @param eventDto event related information
	 */
	@PostMapping("/bulletinBoard/trackVote")
	public void trackVote(@RequestBody BulletinBoardEventDto eventDto, @RequestHeader("Authorization") String apiKey) {
		Optional<Vote> vote = voteRepository.findById(eventDto.getVoteId());
		BulletinBoard bulletinBoard = permissions.getAuthorizedBulletinBoard(apiKey);

		if (vote.isPresent() && bulletinBoardEvents.contains(eventDto.getType())) {
			trackingRepository.save(new BulletinBoardEvent(0, vote.get(), bulletinBoard, Calendar.getInstance(), eventDto.getType()));
		}
	}

	/**
	 * Informs Bulletin Board over the state of a vote. Used to determine weather a vote can be counted.
	 *
	 * @param voteId id of a Vote
	 * @param apiKey ApiKey of a valid Bulletin board
	 * @return List of events for a vote
	 */
	@GetMapping("/bulletinBoard/checkVoteState/{voteId}")
	public List<EventResponseDto> trackVote(@PathVariable(value = "voteId") long voteId, @RequestHeader("Authorization") String apiKey) {
		permissions.getAuthorizedBulletinBoard(apiKey);
		Optional<Vote> vote = voteRepository.findById(voteId);

		if (vote.isPresent()) {
			List<VoteEvent> voteEvents = trackingRepository.findAllByVote(vote.get());
			return getEventResponseDtos(voteEvents);
		}
		return new ArrayList<>();
	}

	private List<EventResponseDto> getEventResponseDtos(List<VoteEvent> voteEvents) {
		List<EventResponseDto> eventResponseDtos = new ArrayList<>();
		for (VoteEvent voteEvent : voteEvents) {
			if (voteEvent instanceof BulletinBoardEvent) {
				eventResponseDtos.add(new EventResponseDto("BulletinBoard", voteEvent.getVote().getId(), voteEvent.getType(), voteEvent.getTime(), ((BulletinBoardEvent) voteEvent).getBulletinBoard().getId()));
			} else {
				eventResponseDtos.add(new EventResponseDto("Voter", voteEvent.getVote().getId(), voteEvent.getType(), voteEvent.getTime(), 0));
			}
		}
		return eventResponseDtos;
	}
}
