package ch.hsr.ba.coordinator.controllers;

import ch.hsr.ba.coordinator.controllers.exceptions.InvalidInputException;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidRequestException;
import ch.hsr.ba.coordinator.controllers.exceptions.ResourceNotFoundException;
import ch.hsr.ba.coordinator.data.PublicKeyRepository;
import ch.hsr.ba.coordinator.data.ResultRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import ch.hsr.ba.coordinator.data.model.vote.BulletinBoardResult;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.MkLweSample;
import ch.hsr.ba.coordinator.data.model.vote.OptionCounter;
import ch.hsr.ba.coordinator.data.model.vote.VoteBulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.data.model.vote.VoteTrustee;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Controller
public class VoteController {
	private final Logger logger = LoggerFactory.getLogger(VoteController.class);
	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	PublicKeyRepository publicKeyRepository;
	@Autowired
	ResultRepository resultRepository;

	/**
	 * assigns a Trustee to a {@link VoteExecution}.
	 *
	 * @param voteId  id of a {@link VoteExecution}
	 * @param trustee Trustee to assign
	 * @return assignment with order id
	 * @throws InvalidRequestException   if the number of trustees is exceeded
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 */
	public VoteTrustee participate(long voteId, Trustee trustee) throws InvalidRequestException, ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		if (ve.getVoteParams() == null) {
			throw new ResourceNotFoundException("Vote not found");
		}
		if (ve.getTrustees().size() < ve.getVoteParams().getTrustees()) {
			ve.getTrustees().add(new VoteTrustee(0, ve.getTrustees().size(), trustee, ve, new ArrayList<>(), false));
			ve = voteExecutionRepository.save(ve);
			for (VoteTrustee voteTrustee : ve.getTrustees()) {
				if (voteTrustee.getTrustee().getId() == trustee.getId()) {
					return voteTrustee;
				}
			}
			throw new InvalidRequestException("Unable to generate valid participation");
		} else {
			throw new InvalidRequestException("Number of trustees is already reached");
		}
	}

	/**
	 * assigns a BulletinBoard to a {@link VoteExecution}.
	 *
	 * @param voteId        id of a {@link VoteExecution}
	 * @param bulletinBoard BulletinBoard to assign
	 * @return assignment
	 * @throws InvalidRequestException   if the number of bulletinBoards is exceeded
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 */
	public VoteBulletinBoard participate(long voteId, BulletinBoard bulletinBoard) throws ResourceNotFoundException, InvalidRequestException {
		VoteExecution ve = getVote(voteId);
		if (ve.getVoteParams() == null) {
			throw new ResourceNotFoundException("Vote not found");
		}

		if (ve.getBulletinBoards().size() < ve.getVoteParams().getBulletinBoards()) {
			ve.getBulletinBoards().add(new VoteBulletinBoard(0, bulletinBoard, ve));
			ve = voteExecutionRepository.save(ve);
			for (VoteBulletinBoard voteBulletinBoard : ve.getBulletinBoards()) {
				if (voteBulletinBoard.getBulletinBoard().getId() == bulletinBoard.getId()) {
					this.checkKeysComplete(voteId);
					return voteBulletinBoard;
				}
			}
			throw new InvalidRequestException("Unable to generate valid participation");
		} else {
			throw new InvalidRequestException("Number of bulletin boards is already reached");
		}
	}

	/**
	 * Publishes the public keys.
	 *
	 * @param voteId     id of a {@link VoteExecution}
	 * @param trustee    trustee sending the keys
	 * @param lweSamples list of public keys ({@link LweSample})
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 * @throws InvalidInputException     if the key structure does not match the params
	 */
	public void addPublicKeys(long voteId, Trustee trustee, List<LweSample> lweSamples) throws ResourceNotFoundException, InvalidInputException {
		VoteExecution ve = getVote(voteId);
		if (!ve.isStarted()) {
			throw new ResourceNotFoundException("Vote not found");
		}
		if (ve.getVoteParams().getGeneratedKeys() != lweSamples.size()) {
			throw new InvalidInputException("Invalid key count");
		}
		for (VoteTrustee voteTrustee : ve.getTrustees()) {
			if (voteTrustee.getTrustee().getId() == trustee.getId()) {
				List<LweSample> lweSampleList = new ArrayList<>();
				for (LweSample lweSample : lweSamples) {
					if (lweSample.getPhaseShift().length != ve.getVoteParams().getSn()) {
						throw new InvalidInputException("Invalid of length");
					}
					lweSampleList.add(new LweSample(0, voteTrustee, lweSample.getPhaseShift(), lweSample.getPhase()));
				}
				publicKeyRepository.saveAll(lweSampleList);
				voteTrustee.setReady(true);
				voteExecutionRepository.save(ve);
				break;
			}
		}
		this.checkKeysComplete(voteId);
	}

	private void checkKeysComplete(long voteId) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		if (ve.getTrustees().size() == ve.getVoteParams().getTrustees()) {
			for (VoteTrustee voteTrustee : ve.getTrustees()) {
				if (voteTrustee.getLweSamples().size() != ve.getVoteParams().getGeneratedKeys()) {
					return;
				}
			}
			if (ve.getBulletinBoards().size() == ve.getVoteParams().getBulletinBoards()) {
				this.startBulletinBoardKeyGeneration(voteId);
			}
		}
	}

	public void startDecryption(long voteId) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		logger.debug("Vote stopped: " + voteId);
		for (VoteBulletinBoard bulletinBoard : ve.getBulletinBoards()) {
			try (CloseableHttpClient client = HttpClients.createDefault()) {
				URI url = new URI("http", null, bulletinBoard.getBulletinBoard().getHostname(), bulletinBoard.getBulletinBoard().getPort(), "/votedone", null, null);
				HttpPost httpPost = new HttpPost(url);
				CloseableHttpResponse response = client.execute(httpPost);
			} catch (IOException | URISyntaxException e) {
				logger.error(e.getLocalizedMessage(), e);
			}
		}
		ve.setDone(true);
		if (ve.getVoteParams().getVoteEnd().after(Calendar.getInstance())) {
			ve.getVoteParams().setVoteEnd(Calendar.getInstance());
		}
		voteExecutionRepository.save(ve);
	}

	private void startBulletinBoardKeyGeneration(long voteId) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		Thread t = new Thread(() -> {
			try {
				Thread.sleep(10000);
			} catch (Exception e) {
			}
			for (VoteBulletinBoard bulletinBoard : ve.getBulletinBoards()) {
				try (CloseableHttpClient client = HttpClients.createDefault()) {
					URI url = new URI("http", null, bulletinBoard.getBulletinBoard().getHostname(), bulletinBoard.getBulletinBoard().getPort(), "/generatekeys", null, null);
					HttpPost httpPost = new HttpPost(url);
					CloseableHttpResponse response = client.execute(httpPost);
				} catch (IOException | URISyntaxException e) {
					logger.error(e.getLocalizedMessage(), e);
				}
			}
		});
		t.start();
	}

	public VoteExecution getVote(long voteId) throws ResourceNotFoundException {
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(voteId);
		if (voteExecution.isPresent()) {
			return voteExecution.get();
		}
		throw new ResourceNotFoundException("invalid vote");
	}

	/**
	 * Sends the totals to the coordinator.
	 *
	 * @param voteId        id of a {@link VoteExecution}
	 * @param bulletinBoard BulletinBoard sending its counters
	 * @param counters      counters
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 */
	public void publishCounters(long voteId, BulletinBoard bulletinBoard, Map<Integer, List<LweSample>> counters) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		for (VoteBulletinBoard board : ve.getBulletinBoards()) {
			if (board.getBulletinBoard().getId() == bulletinBoard.getId()) {
				List<OptionCounter> optionCounters = new ArrayList<>();

				for (Map.Entry<Integer, List<LweSample>> counter : counters.entrySet()) {
					for (VoteChoices choices : ve.getChoices()) {
						if (choices.getOrder() == counter.getKey()) {
							List<MkLweSample> mkLweSamples = new ArrayList<>();
							for (LweSample lweSample : counter.getValue()) {
								mkLweSamples.add(new MkLweSample(0, lweSample.getPhaseShift(), lweSample.getPhase(), ve.getVoteParams().getTrustees()));
							}
							optionCounters.add(new OptionCounter(0, choices, mkLweSamples, false, 0));
							break;
						}
					}
				}
				board.setBulletinBoardResult(resultRepository.save(new BulletinBoardResult(0, board, optionCounters)));
				voteExecutionRepository.save(ve);
				break;
			}
		}
	}

	/**
	 * Trustee publishing its decryption.
	 *
	 * @param voteId          id of a {@link VoteExecution}
	 * @param bulletinboardId bulletin board id the partial decryption values belong to
	 * @param trustee         Trustee sending the decryption
	 * @param results         partial decryption values
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 */
	public void publishResults(long voteId, long bulletinboardId, Trustee trustee, List<List<Integer>> results) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		VoteTrustee vt = null;
		for (VoteTrustee voteTrustee : ve.getTrustees()) {
			if (voteTrustee.getTrustee().getId() == trustee.getId()) {
				vt = voteTrustee;
				break;
			}
		}
		if (vt == null) {
			throw new ResourceNotFoundException("invalid trustee");
		}
		for (VoteBulletinBoard board : ve.getBulletinBoards()) {
			if (board.getBulletinBoard().getId() == bulletinboardId) {
				Optional<BulletinBoardResult> boardResult = resultRepository.findByVoteBulletinBoard(board);
				if (boardResult.isPresent()) {
					BulletinBoardResult br = boardResult.get();
					for (int choice = 0; choice < ve.getChoices().size(); choice++) {
						List<MkLweSample> counterSamples = br.getSamples().get(choice).getMkLweSamples();
						for (int bit = 0; bit < counterSamples.size(); bit++) {
							counterSamples.get(bit).getDecryption()[vt.getTrusteeOrder()] = results.get(choice).get(bit);
							counterSamples.get(bit).getDecrypted()[vt.getTrusteeOrder()] = true;
						}
					}
					resultRepository.save(br);
				} else {
					throw new ResourceNotFoundException("invalid board Id");
				}
				break;
			}
		}
	}

	/**
	 * Bulletin board announcing its readystate.
	 *
	 * @param voteId        id of a {@link VoteExecution}
	 * @param bulletinBoard BulletinBoard which has reached the readystate
	 * @throws ResourceNotFoundException if vote doesnt exist or is not available
	 */
	public void setReady(long voteId, BulletinBoard bulletinBoard) throws ResourceNotFoundException {
		VoteExecution ve = getVote(voteId);
		for (VoteBulletinBoard board : ve.getBulletinBoards()) {
			if (board.getBulletinBoard().getId() == bulletinBoard.getId()) {
				board.setReady(true);
				voteExecutionRepository.save(ve);
				return;
			}
		}
	}

	public List<VoteExecution> getVotes() {
		return voteExecutionRepository.findAllActive();
	}

	/**
	 * Lists all VoteExecutions a bulletin board could subscribe to.
	 *
	 * @param bulletinBoard Bulletin board looking for an assignment
	 * @return list of valid assignments
	 */
	public List<VoteExecution> getVotes(BulletinBoard bulletinBoard) {
		List<VoteExecution> allInPreparation = voteExecutionRepository.findAllInPreparation();
		List<VoteExecution> available = new ArrayList<>();
		for (VoteExecution voteExecution : allInPreparation) {
			if (voteExecution.getBulletinBoards().size() < voteExecution.getVoteParams().getBulletinBoards()) {
				boolean isContained = false;
				for (VoteBulletinBoard board : voteExecution.getBulletinBoards()) {
					if (bulletinBoard.getId() == board.getBulletinBoard().getId()) {
						isContained = true;
						break;
					}
				}
				if (!isContained) {
					available.add(voteExecution);
				}
			}
		}
		return available;
	}

	/**
	 * Lists all VoteExecutions a Trustee could subscribe to.
	 *
	 * @param trustee Trustee looking for an assignment
	 * @return list of valid assignments
	 */
	public List<VoteExecution> getVotes(Trustee trustee) {
		List<VoteExecution> allInPreparation = voteExecutionRepository.findAllInPreparation();
		List<VoteExecution> available = new ArrayList<>();
		for (VoteExecution voteExecution : allInPreparation) {
			if (voteExecution.getTrustees().size() < voteExecution.getVoteParams().getTrustees()) {
				boolean isContained = false;
				for (VoteTrustee voteTrustee : voteExecution.getTrustees()) {
					if (trustee.getId() == voteTrustee.getTrustee().getId()) {
						isContained = true;
						break;
					}
				}
				if (!isContained) {
					available.add(voteExecution);
				}
			}
		}
		return available;
	}
}
