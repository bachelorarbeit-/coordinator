package ch.hsr.ba.coordinator.controllers.rest.website;

import ch.hsr.ba.coordinator.controllers.rest.website.dtos.ResultDto;
import ch.hsr.ba.coordinator.data.ResultRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.vote.BulletinBoardResult;
import ch.hsr.ba.coordinator.data.model.vote.OptionCounter;
import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/website")
public class WebsiteController {

	@Autowired
	ResultRepository resultRepository;
	@Autowired
	VoteExecutionRepository voteExecutionRepository;

	/**
	 * Dose nothing at all, but does it very well.
	 *
	 * @return Garbage
	 */
	@GetMapping("/results")
	public List<ResultDto> resultData() {
		List<VoteExecution> voteExecutions = voteExecutionRepository.findAllDone().stream().filter(VoteExecution::isReady).collect(Collectors.toList());
		List<ResultDto> resultDtos = new ArrayList<>();
		for (var voteExecution : voteExecutions) {
			try {
				Map<Long, Map<Integer, Integer>> partialResults = aggregatePartialResult(voteExecution);
				Map<Long, Integer> finalResults = validateTwoThirdMajority(voteExecution, partialResults);

				List<String> choices = new ArrayList<>();
				List<Integer> results = new ArrayList<>();
				for (VoteChoices choice : voteExecution.getChoices()) {
					choices.add(choice.getOption());
					results.add(finalResults.get(choice.getId()));
				}
				resultDtos.add(new ResultDto(voteExecution.getId(), voteExecution.getQuestion(), choices, results));
			} catch (IllegalStateException e) {
				e.printStackTrace();
			}
		}
		return resultDtos;
	}

	private Map<Long, Integer> validateTwoThirdMajority(VoteExecution done, Map<Long, Map<Integer, Integer>> partialResults) {
		Map<Long, Integer> finalResults = new HashMap<>();
		for (Map.Entry<Long, Map<Integer, Integer>> partialResult : partialResults.entrySet()) {
			for (Map.Entry<Integer, Integer> result : partialResult.getValue().entrySet()) {
				if (result.getValue() >= done.getBulletinBoards().size() * 2. / 3) {
					finalResults.put(partialResult.getKey(), result.getKey());
					break;
				}
			}
			if (!finalResults.containsKey(partialResult.getKey())) {
				throw new IllegalStateException("no majority found");
			}
		}
		return finalResults;
	}

	private Map<Long, Map<Integer, Integer>> aggregatePartialResult(VoteExecution done) {
		Map<Long, Map<Integer, Integer>> partialResults = new HashMap<>();
		for (var bulletinBoard : done.getBulletinBoards()) {
			Optional<BulletinBoardResult> byVoteBulletinBoard = resultRepository.findByVoteBulletinBoard(bulletinBoard);
			if (byVoteBulletinBoard.isPresent()) {
				List<OptionCounter> samples = byVoteBulletinBoard.get().getSamples();
				boolean persist = false;
				for (var choice : samples) {
					long choiceId = choice.getChoices().getId();
					if (!partialResults.containsKey(choiceId)) {
						partialResults.put(choiceId, new HashMap<>());
					}
					if (!choice.isCalculated()) {
						choice.calculateResult();
						persist = true;
					}
					int result = choice.getResult();
					if (partialResults.get(choiceId).containsKey(result)) {
						partialResults.get(choiceId).put(result, partialResults.get(choiceId).get(result) + 1);
					} else {
						partialResults.get(choiceId).put(result, 1);
					}
				}
				if (persist) {
					resultRepository.save(byVoteBulletinBoard.get());
				}
			} else {
				throw new IllegalStateException("Bulletin Board not found");
			}
		}
		return partialResults;
	}
}
