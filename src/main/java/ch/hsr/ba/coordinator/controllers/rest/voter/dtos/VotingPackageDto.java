package ch.hsr.ba.coordinator.controllers.rest.voter.dtos;

import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;
import ch.hsr.ba.coordinator.data.model.vote.VoteParams;

import java.util.List;
import java.util.Map;

public class VotingPackageDto {
	private final long voteId;
	private final String question;
	private final List<VoteChoices> choices;

	private final List<LocationDto> bulletinBoards;
	private final VoteParams params;
	private final Map<Integer, List<LweSample>> trustees;

	/**
	 * Default constructor.
	 */
	public VotingPackageDto() {
		this.voteId = 0;
		this.question = null;
		this.choices = null;
		this.bulletinBoards = null;
		this.params = null;
		this.trustees = null;
	}

	/**
	 * constructor for all values.
	 *
	 * @param voteId         id of the VoteExecution
	 * @param question       question to bw asked
	 * @param choices        answers to the question
	 * @param bulletinBoards list of Bulletin boards
	 * @param params         Vote Parameters
	 * @param trustees       list of public Keys
	 */
	public VotingPackageDto(long voteId, String question, List<VoteChoices> choices, List<LocationDto> bulletinBoards, VoteParams params, Map<Integer, List<LweSample>> trustees) {
		this.voteId = voteId;
		this.question = question;
		this.choices = choices;
		this.bulletinBoards = bulletinBoards;
		this.params = params;
		this.trustees = trustees;
	}

	public long getVoteId() {
		return voteId;
	}

	public String getQuestion() {
		return question;
	}

	public List<VoteChoices> getChoices() {
		return choices;
	}

	public List<LocationDto> getBulletinBoards() {
		return bulletinBoards;
	}

	public VoteParams getParams() {
		return params;
	}

	public Map<Integer, List<LweSample>> getTrustees() {
		return trustees;
	}
}
