package ch.hsr.ba.coordinator.controllers.rest.tracker.dtos;

import java.util.Calendar;

public class EventResponseDto extends VoteEventDto {
	private final String origin;
	private final long bulletinboardId;
	private final Calendar time;

	public EventResponseDto() {
		super(0, null);
		this.time = null;
		this.bulletinboardId = 0;
		this.origin = null;
	}

	public EventResponseDto(String origin, long voteId, String type, Calendar time, long bulletinboardId) {
		super(voteId, type);
		this.time = time;
		this.bulletinboardId = bulletinboardId;
		this.origin = origin;
	}

	public String getOrigin() {
		return origin;
	}

	public long getBulletinboardId() {
		return bulletinboardId;
	}

	public Calendar getTime() {
		return time;
	}
}
