package ch.hsr.ba.coordinator.controllers.exceptions;

public class ResourceNotFoundException extends Exception {
	public ResourceNotFoundException(String message) {
		super(message);
	}
}
