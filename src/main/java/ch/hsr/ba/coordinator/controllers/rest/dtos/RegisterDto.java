package ch.hsr.ba.coordinator.controllers.rest.dtos;

public class RegisterDto {
	private final long id;
	private final String apiKey;

	public RegisterDto() {
		this.id = 0;
		this.apiKey = null;
	}

	public RegisterDto(long id, String apiKey) {
		this.id = id;
		this.apiKey = apiKey;
	}

	public long getId() {
		return id;
	}

	public String getApiKey() {
		return apiKey;
	}
}
