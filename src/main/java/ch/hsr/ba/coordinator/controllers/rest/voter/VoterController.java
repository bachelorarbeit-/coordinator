package ch.hsr.ba.coordinator.controllers.rest.voter;

import ch.hsr.ba.coordinator.controllers.PermissionController;
import ch.hsr.ba.coordinator.controllers.rest.dtos.RegisterDto;
import ch.hsr.ba.coordinator.controllers.rest.dtos.VoteExecutionDto;
import ch.hsr.ba.coordinator.controllers.rest.voter.dtos.LocationDto;
import ch.hsr.ba.coordinator.controllers.rest.voter.dtos.VotingPackageDto;
import ch.hsr.ba.coordinator.data.TrackingRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.VoteRepository;
import ch.hsr.ba.coordinator.data.model.authentication.Voter;
import ch.hsr.ba.coordinator.data.model.tracking.BulletinBoardEvent;
import ch.hsr.ba.coordinator.data.model.tracking.Vote;
import ch.hsr.ba.coordinator.data.model.tracking.VoteEvent;
import ch.hsr.ba.coordinator.data.model.tracking.VoterEvent;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteBulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/voter")
public class VoterController {
	@Autowired
	PermissionController permissions;
	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	VoteRepository voteRepository;
	@Autowired
	TrackingRepository trackingRepository;


	@RequestMapping("/register")
	public RegisterDto register() {
		String apiKey = PasswordUtil.generateApiKey(64);
		Voter voter = permissions.addVoter(apiKey);
		return new RegisterDto(voter.getId(), apiKey);
	}

	@GetMapping("/getVotes")
	public List<VoteExecutionDto> getVotes(@RequestHeader("Authorization") String apiKey) {
		Voter voter = permissions.getAuthorizedVoter(apiKey);

		List<Vote> votes = voteRepository.findByVoter(voter);
		List<Long> voteIds = new ArrayList<>();
		for(Vote vote : votes) {
			List<VoteEvent> events = trackingRepository.findAllByVote(vote);
			for (VoteEvent event : events) {
				if(event instanceof BulletinBoardEvent || event.getType().equalsIgnoreCase("voteSent")) {
					voteIds.add(vote.getVoteExecution().getId());
				}
			}
		}

		List<VoteExecution> voteExecutions = voteExecutionRepository.findAllActive().stream().filter(voteExecution -> !voteIds.contains(voteExecution.getId())).collect(Collectors.toList());

		List<VoteExecutionDto> dtos = new ArrayList<>();
		for (VoteExecution ve : voteExecutions) {
			dtos.add(new VoteExecutionDto(ve.getId(), ve.getQuestion(), ve.getChoices()));
		}
		return dtos;
	}

	@GetMapping("/participations")
	public List<VoteExecutionDto> getParticipations(@RequestHeader("Authorization") String apiKey) {
		Voter voter = permissions.getAuthorizedVoter(apiKey);
		List<VoteExecutionDto> dtos = new ArrayList<>();
		for (VoteExecution ve : voteExecutionRepository.findAllParticipations(voter)) {
			dtos.add(new VoteExecutionDto(ve.getId(), ve.getQuestion(), ve.getChoices()));
		}
		return dtos;
	}

	/**
	 * generates the voting package for a Voter.
	 *
	 * @param voteId id of a {@link VoteExecution}
	 * @param apiKey Valid Voter ApiKey
	 * @return required information for the Voter {@link VotingPackageDto}
	 */
	@RequestMapping("/votingpackage/{voteId}")
	public VotingPackageDto getVotingPackage(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey) {
		Voter voter = permissions.getAuthorizedVoter(apiKey);
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(voteId);
		if (voteExecution.isPresent() && voteExecution.get().isOpen()) {
			VoteExecution ve = voteExecution.get();

			Optional<Vote> voteOptional = voteRepository.findByVoterAndVote(voter, ve);
			Vote vote = null;
			if (voteOptional.isPresent()) {
				vote = voteOptional.get();
			} else {
				vote = voteRepository.save(new Vote(0, voter, ve));
			}

			List<LocationDto> locationDtos = new ArrayList<>();
			for (VoteBulletinBoard board : ve.getBulletinBoards()) {
				locationDtos.add(new LocationDto(board.getBulletinBoard().getHostname(), board.getBulletinBoard().getPort()));
			}

			Map<Integer, List<LweSample>> trustees = new HashMap<>();
			for (int i = 0; i < ve.getVoteParams().getTrustees(); i++) {
				trustees.put(ve.getTrustees().get(i).getTrusteeOrder(), ve.getTrustees().get(i).getLweSamples());
			}

			return new VotingPackageDto(vote.getId(), ve.getQuestion(), ve.getChoices(), locationDtos, ve.getVoteParams(), trustees);
		} else {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Cant find vote");
		}
	}
}
