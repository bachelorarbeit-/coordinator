package ch.hsr.ba.coordinator.controllers.rest.trustee;

import ch.hsr.ba.coordinator.controllers.PermissionController;
import ch.hsr.ba.coordinator.controllers.VoteController;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidInputException;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidRequestException;
import ch.hsr.ba.coordinator.controllers.exceptions.ResourceNotFoundException;
import ch.hsr.ba.coordinator.controllers.rest.dtos.RegisterDto;
import ch.hsr.ba.coordinator.controllers.rest.dtos.VoteExecutionDto;
import ch.hsr.ba.coordinator.controllers.rest.trustee.dtos.ComponentDataDto;
import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.utils.PasswordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/trustee")
public class TrusteeController {
	Logger logger = LoggerFactory.getLogger(TrusteeController.class);

	@Autowired
	PermissionController permissions;
	@Autowired
	VoteController voteController;


	@PostMapping("/register")
	public RegisterDto register(@RequestBody ComponentDataDto td) {
		String apiKey = PasswordUtil.generateApiKey(64);
		Trustee trustee = permissions.addTrustee(apiKey, td.getHostname(), td.getPort());
		return new RegisterDto(trustee.getId(), apiKey);
	}

	/**
	 * returns all available VoteExecutions on which a Trustee could register.
	 *
	 * @param apiKey Trustee ApiKey
	 * @return list of VoteExecutions
	 */
	@PostMapping("/getVotes")
	public List<VoteExecutionDto> getVotes(@RequestHeader("Authorization") String apiKey) {
		Trustee trustee = permissions.getAuthorizedTrustee(apiKey);
		List<VoteExecutionDto> dtos = new ArrayList<>();
		for (VoteExecution ve : voteController.getVotes(trustee)) {
			dtos.add(new VoteExecutionDto(ve.getId(), ve.getQuestion(), ve.getChoices()));
		}
		return dtos;
	}

	/**
	 * registers a Trustee to a VoteExecution.
	 *
	 * @param voteId id of the VoteExecution
	 * @param apiKey Trustee ApiKey
	 * @return returns the VoteExecution with all required details
	 */
	@PostMapping("/registerToVote/{voteId}")
	public VoteExecution registerToVote(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey) {
		Trustee trustee = permissions.getAuthorizedTrustee(apiKey);
		try {
			voteController.participate(voteId, trustee);
			return voteController.getVote(voteId);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		} catch (InvalidRequestException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Publishes public Keys to Coordinator for distribution.
	 *
	 * @param voteId     {@link VoteExecution} id
	 * @param apiKey     valid ApiKey of Trustee
	 * @param lweSamples List of public keys ({@link LweSample})
	 */
	@PostMapping("/publishKeys/{voteId}")
	public void publishKeys(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey, @RequestBody List<LweSample> lweSamples) {
		Trustee trustee = permissions.getAuthorizedTrustee(apiKey);
		try {
			voteController.addPublicKeys(voteId, trustee, lweSamples);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		} catch (InvalidInputException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Publishes the Partial encryption values.
	 *
	 * @param voteId          {@link VoteExecution} id
	 * @param bulletinboardId id of a BulletinBoard to which the decryption belongs
	 * @param apiKey          valid ApiKey of Trustee
	 * @param results         List of counters (List of Integers),
	 */
	@PostMapping("/results/{voteId}/{bulletinboardId}")
	public void receiveResults(@PathVariable long voteId, @PathVariable long bulletinboardId, @RequestHeader("Authorization") String apiKey, @RequestBody List<List<Integer>> results) {
		Trustee trustee = permissions.getAuthorizedTrustee(apiKey);
		try {
			voteController.publishResults(voteId, bulletinboardId, trustee, results);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}
	}
}
