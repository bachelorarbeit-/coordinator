package ch.hsr.ba.coordinator.controllers;

import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.TrusteeRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.authentication.PermissionComponent;
import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Optional;

@Service
public class HeartBeatService {
	Logger log = LoggerFactory.getLogger(HeartBeatService.class);

	@Autowired
	BulletinBoardRepository bulletinBoardRepository;
	@Autowired
	TrusteeRepository trusteeRepository;


	/**
	 * Checks periodically for the availability of other components (trustee, bulletin boards).
	 * fixedDelay => waits 5 seconds after successful execution before starting next cycle.
	 */
	@Scheduled(fixedDelay = 5000)
	public void checkComponentHealth() {
		for (Trustee trustee : trusteeRepository.findAll()) {
			this.checkComponentHealth(trustee, trusteeRepository);
		}
		for (BulletinBoard bulletinBoard : bulletinBoardRepository.findAll()) {
			this.checkComponentHealth(bulletinBoard, bulletinBoardRepository);
		}
	}

	private <C extends PermissionComponent, R extends JpaRepository<C, Long>> void checkComponentHealth(C component, R repository) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		try {
			URI uri = new URI("http", null, component.getHostname(), component.getPort(), "/heartbeat", null, null);
			HttpGet httpGet = new HttpGet(uri);
			CloseableHttpResponse response = httpClient.execute(httpGet);
			Optional<C> c = repository.findById(component.getId());
			if (c.isPresent()) {
				if (response.getStatusLine().getStatusCode() == 200) {
					c.get().setLastContact(Calendar.getInstance());
					c.get().setRunning(true);
				} else {
					c.get().setRunning(false);
				}
				repository.save(c.get());
			}
		} catch (URISyntaxException e) {
			log.error(e.getMessage(), e);
		} catch (IOException e) {
			log.debug(e.getMessage());
		}
	}
}
