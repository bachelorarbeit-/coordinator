package ch.hsr.ba.coordinator.controllers.web;

import ch.hsr.ba.coordinator.controllers.rest.website.dtos.ResultDto;
import ch.hsr.ba.coordinator.data.ResultRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/results")
public class ResultController {

	@Autowired
	ResultRepository resultRepository;
	@Autowired
	VoteExecutionRepository voteExecutionRepository;

	@RequestMapping("")
	public String overview(Model model) {
		List<ResultDto> resultDtos = getResults();

		model.addAttribute("resultData", resultDtos);
		return "website/views/results.html";
	}

	/**
	 * Returns Finalized votes doesn't check for result availability
	 *
	 * @return Finalized votes
	 */
	public List<ResultDto> getResults() {
		List<VoteExecution> voteExecutions = voteExecutionRepository.findAllDone().stream().filter(VoteExecution::isReady).collect(Collectors.toList());
		List<ResultDto> resultDtos = new ArrayList<>();
		for (var voteExecution : voteExecutions) {
			resultDtos.add(new ResultDto(voteExecution.getId(), voteExecution.getQuestion(), null, null));
		}
		return resultDtos;
	}
}