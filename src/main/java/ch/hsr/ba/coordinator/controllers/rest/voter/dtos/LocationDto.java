package ch.hsr.ba.coordinator.controllers.rest.voter.dtos;

public class LocationDto {
	private final String host;
	private final int port;

	public LocationDto() {
		this.host = null;
		this.port = 0;
	}

	public LocationDto(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public int getPort() {
		return port;
	}
}
