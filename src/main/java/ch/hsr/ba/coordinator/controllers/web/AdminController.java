package ch.hsr.ba.coordinator.controllers.web;

import ch.hsr.ba.coordinator.controllers.VoteController;
import ch.hsr.ba.coordinator.controllers.exceptions.ResourceNotFoundException;
import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.TrusteeRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.VoteParamsRepository;
import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.data.model.vote.VoteParams;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	VoteParamsRepository voteParamsRepository;
	@Autowired
	TrusteeRepository trusteeRepository;
	@Autowired
	BulletinBoardRepository bulletinBoardRepository;
	@Autowired
	VoteController voteController;

	@RequestMapping("")
	public String overview(Model model) {
		model.addAttribute("votes", voteExecutionRepository.findAll());
		model.addAttribute("trustees", trusteeRepository.findAll());
		model.addAttribute("bulletinBoards", bulletinBoardRepository.findAll());
		return "admin/index.html";
	}

	/**
	 * Displays basic Vote details.
	 *
	 * @param id    id of a VoteExecution
	 * @param model Thymeleaf data model
	 * @return template uri
	 */
	@RequestMapping("/votes/{id}")
	public String votesView(@PathVariable long id, Model model) {
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(id);
		if (voteExecution.isPresent()) {
			model.addAttribute("vote", voteExecution.get());
		}
		return "admin/votesView.html";
	}

	/**
	 * Starts a vote => Params can no longer be edited, but it gets visible for Trustees and BBs.
	 *
	 * @param id    id of a VoteExecution
	 * @param model Thymeleaf data model
	 * @return 		Redirect to admin page
	 */
	@RequestMapping("/startVote/{id}")
	public RedirectView startVote(@PathVariable long id, Model model) {
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(id);
		if (voteExecution.isPresent()) {
			voteExecution.get().start();
			voteExecution.get().generateRLWEKey();
			voteExecutionRepository.save(voteExecution.get());
		}
		return new RedirectView("/admin/votes/" + id);
	}

	/**
	 * Ends a Vote and starts Decryption.
	 *
	 * @param id    id of a VoteExecution
	 * @param model Thymeleaf data model
	 * @return 		Redirect to admin page
	 */
	@RequestMapping("/endVote/{id}")
	public RedirectView endVote(@PathVariable long id, Model model) {
		try {
			voteController.startDecryption(id);
		} catch (ResourceNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}
		return new RedirectView("/admin/votes/" + id);
	}

	@GetMapping("/addVote")
	public String addVote() {
		return "admin/addVote.html";
	}

	/**
	 * Creates a new voteExecution.
	 *
	 * @param voteExecution VoteExecution to save
	 */
	@PostMapping("/addVote")
	public void addVote(@RequestBody VoteExecution voteExecution) {
		List<VoteChoices> choices = new ArrayList<>();
		VoteExecution vote = new VoteExecution(0, voteExecution.getQuestion(), choices);
		for (VoteChoices choice : voteExecution.getChoices()) {
			choices.add(new VoteChoices(0, choice.getOrder(), choice.getOption(), vote));
		}
		vote.setVoteParams(new VoteParams(voteExecution.getVoteParams(), vote));
		voteExecutionRepository.save(vote);
	}

	@GetMapping("/edit/{id}")
	public String editVote(@PathVariable long id, Model model) {
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(id);
		if (voteExecution.isPresent()) {
			model.addAttribute("vote", voteExecution.get());
		}
		return "admin/addVote.html";
	}

	/**
	 * Edits a voteExecution.
	 *
	 * @param id            id of the VoteExecution to change
	 * @param voteExecution VoteExecution with changed parameters
	 * @param model         Thymeleaf data model
	 * @return template uri
	 */
	@PostMapping("/edit/{id}")
	public String editVote(@PathVariable long id, @RequestBody VoteExecution voteExecution, Model model) {
		Optional<VoteExecution> optVoteExecution = voteExecutionRepository.findById(id);
		if (optVoteExecution.isPresent()) {
			VoteExecution ve = optVoteExecution.get();
			VoteParams params = ve.getVoteParams();
			params.update(voteExecution.getVoteParams());
			voteParamsRepository.save(params);

			model.addAttribute("vote", voteExecution);
		}
		return "admin/addVote.html";
	}
}
