package ch.hsr.ba.coordinator.controllers.rest.tracker.dtos;

public class BulletinBoardEventDto extends VoteEventDto {
	private final long bulletinBoardId;

	public BulletinBoardEventDto(long voteId, long bulletinBoardId, String type) {
		super(voteId, type);
		this.bulletinBoardId = bulletinBoardId;
	}

	public long getBulletinBoardId() {
		return bulletinBoardId;
	}
}
