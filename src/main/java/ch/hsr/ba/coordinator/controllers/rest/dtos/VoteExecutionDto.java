package ch.hsr.ba.coordinator.controllers.rest.dtos;

import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;

import java.util.List;

public class VoteExecutionDto {
	private final long voteId;
	private final String question;

	private final List<VoteChoices> choices;

	public VoteExecutionDto() {
		this.voteId = 0;
		this.question = "";
		this.choices = null;
	}

	public VoteExecutionDto(long voteId, String question, List<VoteChoices> choices) {
		this.voteId = voteId;
		this.question = question;
		this.choices = choices;
	}

	public long getVoteId() {
		return voteId;
	}

	public String getQuestion() {
		return question;
	}

	public List<VoteChoices> getChoices() {
		return choices;
	}
}
