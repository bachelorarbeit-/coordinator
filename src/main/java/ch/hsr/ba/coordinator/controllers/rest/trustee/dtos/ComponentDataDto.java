package ch.hsr.ba.coordinator.controllers.rest.trustee.dtos;

public class ComponentDataDto {
	private final String hostname;
	private final int port;

	public ComponentDataDto() {
		this.hostname = null;
		this.port = 0;
	}

	public ComponentDataDto(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}

	public String getHostname() {
		return hostname;
	}

	public int getPort() {
		return port;
	}
}
