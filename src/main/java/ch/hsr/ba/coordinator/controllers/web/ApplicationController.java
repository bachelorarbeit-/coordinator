package ch.hsr.ba.coordinator.controllers.web;

import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/applications")
public class ApplicationController {

	@Autowired
	VoteExecutionRepository voteExecutionRepository;

	@RequestMapping("")
	public String overview(Model model) {
		model.addAttribute("voteExecutions", voteExecutionRepository.findAll());
		return "website/views/applications.html";
	}
}