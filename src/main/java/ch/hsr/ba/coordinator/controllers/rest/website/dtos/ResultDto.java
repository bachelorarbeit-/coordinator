package ch.hsr.ba.coordinator.controllers.rest.website.dtos;

import java.util.ArrayList;
import java.util.List;

public class ResultDto {
	private final long id;
	private final String voteQuestion;
	private final List<String> labels;
	private final List<Integer> data;

	public ResultDto() {
		id = 0;
		voteQuestion = "";
		labels = new ArrayList<>();
		data = new ArrayList<>();
	}

	public ResultDto(long id, String voteQuestion, List<String> labels, List<Integer> data) {
		this.id = id;
		this.voteQuestion = voteQuestion;
		this.labels = labels;
		this.data = data;
	}

	public long getId() {
		return id;
	}

	public String getVoteQuestion() {
		return voteQuestion;
	}

	public List<String> getLabels() {
		return labels;
	}

	public List<Integer> getData() {
		return data;
	}
}
