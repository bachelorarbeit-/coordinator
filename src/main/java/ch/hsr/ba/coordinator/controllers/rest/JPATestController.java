package ch.hsr.ba.coordinator.controllers.rest;

import ch.hsr.ba.coordinator.controllers.VoteController;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidInputException;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidRequestException;
import ch.hsr.ba.coordinator.controllers.exceptions.ResourceNotFoundException;
import ch.hsr.ba.coordinator.controllers.rest.tracker.dtos.EventResponseDto;
import ch.hsr.ba.coordinator.data.BulletinBoardRepository;
import ch.hsr.ba.coordinator.data.PublicKeyRepository;
import ch.hsr.ba.coordinator.data.TrusteeRepository;
import ch.hsr.ba.coordinator.data.VoteExecutionRepository;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.authentication.Trustee;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteBulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.VoteChoices;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.data.model.vote.VoteParams;
import ch.hsr.ba.coordinator.data.model.vote.VoteTrustee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

@RestController
@RequestMapping("/test")
public class JPATestController {
	Logger logger = LoggerFactory.getLogger(JPATestController.class);

	@Autowired
	TrusteeRepository trusteeRepository;
	@Autowired
	BulletinBoardRepository bulletinBoardRepository;
	@Autowired
	VoteExecutionRepository voteExecutionRepository;
	@Autowired
	VoteController voteController;
	@Autowired
	PublicKeyRepository publicKeyRepository;

	@RequestMapping("/getVotes")
	public List<VoteExecution> getVote() {
		return voteExecutionRepository.findAll();
	}

	/**
	 * Creates a default Vote execution for testing.
	 *
	 * @return Generated Vote execution
	 */
	@RequestMapping("/initVote")
	public VoteExecution initVote() {
		List<VoteChoices> choices = new ArrayList<>();
		VoteExecution vote = new VoteExecution(0, "Which is the best programming language?", choices);
		int i = 0;
		choices.add(new VoteChoices(0, ++i, "C++", vote));
		choices.add(new VoteChoices(0, ++i, "Java", vote));
		choices.add(new VoteChoices(0, ++i, "Python", vote));
		choices.add(new VoteChoices(0, ++i, "Haskell", vote));
		choices.add(new VoteChoices(0, ++i, "Prolog", vote));
		choices.add(new VoteChoices(0, ++i, "Enthalten", vote));

		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, 1);
		Calendar end = Calendar.getInstance();
		end.add(Calendar.HOUR, 2);
		vote.setVoteParams(new VoteParams(vote, 3, 3, choices.size(), start, end));
		vote.generateRLWEKey();
		return voteExecutionRepository.save(vote);
	}

	/**
	 * Creates a running Vote execution for testing.
	 *
	 * @return Generated Vote execution
	 */
	@RequestMapping("/initRunningVote")
	public VoteExecution initRunningVote() {
		List<VoteChoices> choices = new ArrayList<>();
		VoteExecution vote = new VoteExecution(0, "Which planet is your favorite?", choices);
		int i = 0;
		choices.add(new VoteChoices(0, ++i, "Mercury", vote));
		choices.add(new VoteChoices(0, ++i, "Venus", vote));
		choices.add(new VoteChoices(0, ++i, "Earth", vote));
		choices.add(new VoteChoices(0, ++i, "Mars", vote));
		choices.add(new VoteChoices(0, ++i, "Jupiter", vote));
		choices.add(new VoteChoices(0, ++i, "Saturn", vote));
		choices.add(new VoteChoices(0, ++i, "Uranus", vote));
		choices.add(new VoteChoices(0, ++i, "Neptune", vote));

		Calendar start = Calendar.getInstance();
		start.add(Calendar.HOUR, -1);
		Calendar end = Calendar.getInstance();
		end.add(Calendar.DATE, 1);
		vote.setVoteParams(new VoteParams(vote, 3, 3, choices.size(), start, end));
		vote.generateRLWEKey();
		vote.start();
		vote = voteExecutionRepository.save(vote);

		try {
			this.addTrustees(vote);
			this.addBulletinBoards(vote);
		} catch (InvalidRequestException | InvalidInputException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}

		return vote;
	}

	/**
	 * Creates a completed Vote execution for testing.
	 *
	 * @return Generated Vote execution
	 */
	@RequestMapping("/initCompletedVote")
	public VoteExecution initCompletedVote() {
		List<VoteChoices> choices = new ArrayList<>();
		VoteExecution vote = new VoteExecution(0, "Which celestial body should be colonized?", choices);
		int i = 0;
		choices.add(new VoteChoices(0, ++i, "Mars", vote));
		choices.add(new VoteChoices(0, ++i, "Venus", vote));
		choices.add(new VoteChoices(0, ++i, "Moon", vote));

		Calendar start = Calendar.getInstance();
		start.add(Calendar.MONTH, -1);
		Calendar end = Calendar.getInstance();
		end.add(Calendar.DATE, -1);
		vote.setVoteParams(new VoteParams(vote, 3, 3, choices.size(), start, end));
		vote.generateRLWEKey();
		vote.start();
		vote.setDone(true);
		vote = voteExecutionRepository.save(vote);

		try {
			this.addTrustees(vote);
			this.addBulletinBoards(vote);

			this.simulateVotes(vote);
		} catch (InvalidRequestException | InvalidInputException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}

		return vote;
	}

	private void addTrustees(VoteExecution vote) throws InvalidRequestException, ResourceNotFoundException, InvalidInputException {
		for (int t = 0; t < vote.getVoteParams().getTrustees(); t++) {
			Trustee trustee = trusteeRepository.save(new Trustee("", "localhost", 30000 + t));
			VoteTrustee voteTrustee = voteController.participate(vote.getId(), trustee);
			voteController.addPublicKeys(vote.getId(), trustee, this.generateKeys(voteTrustee, vote.getVoteParams()));
		}
	}

	private List<LweSample> generateKeys(VoteTrustee voteTrustee, VoteParams voteParams) {
		List<LweSample> lweSamples = new ArrayList<>();
		SecureRandom random = new SecureRandom();
		for (int i = 0; i < voteParams.getGeneratedKeys(); i++) {
			Integer[] phaseShift = new Integer[voteParams.getSn()];
			for (int n = 0; n < phaseShift.length; n++) {
				phaseShift[n] = random.nextInt();
			}
			lweSamples.add(new LweSample(0, voteTrustee, phaseShift, Arrays.stream(phaseShift).reduce(0, (a, b) -> a + b)));
		}
		return lweSamples;
	}

	private void addBulletinBoards(VoteExecution vote) throws ResourceNotFoundException, InvalidRequestException {
		for (int bb = 0; bb < vote.getVoteParams().getBulletinBoards(); bb++) {
			BulletinBoard bulletinBoard = bulletinBoardRepository.save(new BulletinBoard("", "localhost", 31000 + bb));
			VoteBulletinBoard voteBulletinBoard = voteController.participate(vote.getId(), bulletinBoard);
			voteController.setReady(vote.getId(), bulletinBoard);
		}
	}

	private void simulateVotes(VoteExecution vote) throws ResourceNotFoundException {
		Map<Integer, Integer> results = new HashMap<>();
		Random random = new Random();
		int maxVotes = 0;
		for (VoteChoices choices : vote.getChoices()) {
			int votes = random.nextInt(100);
			results.put(choices.getOrder(), votes);
			if (votes > maxVotes) {
				maxVotes = votes;
			}
		}
		int counterLength = (int) Math.ceil(Math.log(maxVotes) / Math.log(2));
		Map<Integer, List<Boolean>> binaryVotes = new HashMap<>();
		for (Map.Entry<Integer, Integer> result : results.entrySet()) {
			int value = result.getValue();
			List<Boolean> binarySamples = new ArrayList<>();
			for (int i = 0; i < counterLength; i++) {
				binarySamples.add((value & (1 << i)) != 0);
			}
			binaryVotes.put(result.getKey(), binarySamples);
		}
		this.generateLweResults(vote.getId(), binaryVotes);
	}

	private void generateLweResults(Long vote, Map<Integer, List<Boolean>> binaryVotes) throws ResourceNotFoundException {
		Optional<VoteExecution> voteExecution = voteExecutionRepository.findById(vote);
		Random random = new Random();
		if (voteExecution.isPresent()) {
			final int True = Integer.MAX_VALUE / 2;
			final int False = Integer.MIN_VALUE / 2;

			VoteExecution ve = voteExecution.get();
			List<VoteBulletinBoard> bulletinBoards = new ArrayList<VoteBulletinBoard>(ve.getBulletinBoards());
			for (VoteBulletinBoard bulletinBoard : bulletinBoards) {
				Map<Trustee, Map<Integer, List<Integer>>> trusteeResults = this.getBulletinBoardResults(ve, binaryVotes, random, True, False, bulletinBoard);
				this.generateTrusteeResults(ve, bulletinBoard.getBulletinBoard().getId(), trusteeResults);
			}
		}
	}

	private Map<Trustee, Map<Integer, List<Integer>>> getBulletinBoardResults(VoteExecution vote, Map<Integer, List<Boolean>> binaryVotes, Random random, int trueValue, int falseValue, VoteBulletinBoard bulletinBoard) throws ResourceNotFoundException {
		Map<VoteTrustee, List<LweSample>> publicKeys = new HashMap<>();
		Map<Integer, List<LweSample>> counters = new HashMap<>();
		Map<Trustee, Map<Integer, List<Integer>>> trusteeResults = new HashMap<>();
		for (VoteChoices choices : vote.getChoices()) {
			List<LweSample> counterSamples = new ArrayList<>();
			List<Boolean> result = binaryVotes.get(choices.getOrder());
			for (Boolean bit : result) {
				Integer[] phaseShift = new Integer[vote.getVoteParams().getTrustees() * vote.getVoteParams().getSn()];
				Arrays.fill(phaseShift, 0);

				int phase = bit ? trueValue : falseValue;

				for (VoteTrustee voteTrustee : vote.getTrustees()) {
					if (!publicKeys.containsKey(voteTrustee)) {
						publicKeys.put(voteTrustee, publicKeyRepository.findAllByVoteTrustee(voteTrustee));
					}

					LweSample lweSample = publicKeys.get(voteTrustee).get(random.nextInt(vote.getVoteParams().getGeneratedKeys()));
					phase += lweSample.getPhase();
					for (int n = 0; n < vote.getVoteParams().getSn(); n++) {
						phaseShift[vote.getVoteParams().getSn() * voteTrustee.getTrusteeOrder() + n] += lweSample.getPhaseShift()[n];
					}
					setTrusteeResultPhase(trusteeResults, choices, voteTrustee.getTrustee(), lweSample.getPhase());
				}
				counterSamples.add(new LweSample(0, null, phaseShift, phase));
			}
			counters.put(choices.getOrder(), counterSamples);
		}
		voteController.publishCounters(vote.getId(), bulletinBoard.getBulletinBoard(), counters);
		return trusteeResults;
	}

	private void setTrusteeResultPhase(Map<Trustee, Map<Integer, List<Integer>>> trusteeResults, VoteChoices choices, Trustee trustee, int phase) {
		if (!trusteeResults.containsKey(trustee)) {
			trusteeResults.put(trustee, new HashMap<>());
		}
		if (!trusteeResults.get(trustee).containsKey(choices.getOrder())) {
			trusteeResults.get(trustee).put(choices.getOrder(), new ArrayList<>());
		}
		trusteeResults.get(trustee).get(choices.getOrder()).add(phase);
	}

	private void generateTrusteeResults(VoteExecution vote, long bulletinBoardId, Map<Trustee, Map<Integer, List<Integer>>> trusteeResults) throws ResourceNotFoundException {
		for (Map.Entry<Trustee, Map<Integer, List<Integer>>> trusteeEntry : trusteeResults.entrySet()) {
			Trustee trustee = trusteeEntry.getKey();
			List<List<Integer>> phases = new ArrayList<>();
			for (int i = 0; i < vote.getChoices().size(); i++) {
				phases.add(trusteeEntry.getValue().get(vote.getChoices().get(i).getOrder()));
			}
			voteController.publishResults(vote.getId(), bulletinBoardId, trustee, phases);
		}
	}

	/**
	 * Generates test events.
	 *
	 * @return list of test events
	 */
	@RequestMapping("/getState")
	public List<EventResponseDto> getEventResponseDtos() {
		List<EventResponseDto> eventResponseDtos = new ArrayList<>();
		eventResponseDtos.add(new EventResponseDto("Voter", 1, "packageReceived", Calendar.getInstance(), 0));
		eventResponseDtos.add(new EventResponseDto("Voter", 1, "voteSent", Calendar.getInstance(), 0));
		Random random = new Random();
		for (int i = 1; i <= 3; i++) {
			eventResponseDtos.add(new EventResponseDto("BulletinBoard", 1, "voteReceived", Calendar.getInstance(), i));
			if (random.nextBoolean()) {
				eventResponseDtos.add(new EventResponseDto("BulletinBoard", 1, "voteDequeued", Calendar.getInstance(), i));
				if (random.nextBoolean()) {
					eventResponseDtos.add(new EventResponseDto("BulletinBoard", 1, "voteProcessed", Calendar.getInstance(), i));
				}
			}

		}
		return eventResponseDtos;
	}
}
