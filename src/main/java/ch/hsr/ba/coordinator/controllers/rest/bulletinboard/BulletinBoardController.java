package ch.hsr.ba.coordinator.controllers.rest.bulletinboard;

import ch.hsr.ba.coordinator.controllers.PermissionController;
import ch.hsr.ba.coordinator.controllers.VoteController;
import ch.hsr.ba.coordinator.controllers.exceptions.InvalidRequestException;
import ch.hsr.ba.coordinator.controllers.exceptions.ResourceNotFoundException;
import ch.hsr.ba.coordinator.controllers.rest.dtos.RegisterDto;
import ch.hsr.ba.coordinator.controllers.rest.dtos.VoteExecutionDto;
import ch.hsr.ba.coordinator.controllers.rest.trustee.dtos.ComponentDataDto;
import ch.hsr.ba.coordinator.data.model.authentication.BulletinBoard;
import ch.hsr.ba.coordinator.data.model.vote.LweSample;
import ch.hsr.ba.coordinator.data.model.vote.VoteExecution;
import ch.hsr.ba.coordinator.utils.PasswordUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bulletinBoard")
public class BulletinBoardController {
	Logger logger = LoggerFactory.getLogger(BulletinBoardController.class);

	@Autowired
	PermissionController permissions;
	@Autowired
	VoteController voteController;

	@PostMapping("/register")
	public RegisterDto register(@RequestBody ComponentDataDto cdd) {
		String apiKey = PasswordUtil.generateApiKey(64);
		BulletinBoard bulletinBoard = permissions.addBulletinBoard(apiKey, cdd.getHostname(), cdd.getPort());
		return new RegisterDto(bulletinBoard.getId(), apiKey);
	}

	/**
	 * returns all available VoteExecutions on which a bulletin board could register.
	 *
	 * @param apiKey Bulletin boards ApiKey
	 * @return list of VoteExecutions
	 */
	@PostMapping("/getVotes")
	public List<VoteExecutionDto> getVotes(@RequestHeader("Authorization") String apiKey) {
		BulletinBoard bulletinBoard = permissions.getAuthorizedBulletinBoard(apiKey);
		List<VoteExecutionDto> dtos = new ArrayList<>();
		for (VoteExecution ve : voteController.getVotes(bulletinBoard)) {
			dtos.add(new VoteExecutionDto(ve.getId(), ve.getQuestion(), ve.getChoices()));
		}
		return dtos;
	}

	/**
	 * registers a Bulletin board to a VoteExecution.
	 *
	 * @param voteId id of the VoteExecution
	 * @param apiKey Bulletin boards ApiKey
	 * @return returns the VoteExecution with all required details
	 */
	@PostMapping("/registerToVote/{voteId}")
	public VoteExecution registerToVote(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey) {
		BulletinBoard bulletinBoard = permissions.getAuthorizedBulletinBoard(apiKey);
		try {
			return voteController.participate(voteId, bulletinBoard).getVoteExecution();
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		} catch (InvalidRequestException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Informs the Coordinator that a Bulletin board is done with the key generation.
	 *
	 * @param voteId id of the VoteExecution
	 * @param apiKey Bulletin boards ApiKey
	 */
	@PostMapping("/readyForVote/{voteId}")
	public void setReady(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey) {
		BulletinBoard bulletinBoard = permissions.getAuthorizedBulletinBoard(apiKey);
		try {
			voteController.setReady(voteId, bulletinBoard);
		} catch (ResourceNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}
	}

	/**
	 * Adds the final counters to VoteExecution.
	 *
	 * @param voteId   id of the VoteExecution
	 * @param apiKey   Bulletin boards ApiKey
	 * @param counters Map of counters where the key is the order id of the choice and the counter index 0 is the least significant bit
	 */
	@PostMapping("/counters/{voteId}")
	public void sendCounters(@PathVariable long voteId, @RequestHeader("Authorization") String apiKey, @RequestBody Map<Integer, List<LweSample>> counters) {
		BulletinBoard bulletinBoard = permissions.getAuthorizedBulletinBoard(apiKey);
		try {
			voteController.publishCounters(voteId, bulletinBoard, counters);
		} catch (ResourceNotFoundException e) {
			logger.error(e.getLocalizedMessage(), e);
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getLocalizedMessage(), e);
		}
	}
}
