package ch.hsr.ba.coordinator.controllers.exceptions;

public class InvalidRequestException extends Exception {
	public InvalidRequestException(String message) {
		super(message);
	}
}
