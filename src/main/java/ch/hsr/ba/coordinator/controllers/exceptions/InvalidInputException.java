package ch.hsr.ba.coordinator.controllers.exceptions;

public class InvalidInputException extends Exception {
	public InvalidInputException(String message) {
		super(message);
	}
}
